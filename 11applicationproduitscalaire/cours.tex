%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2016 Louis Paternault --- http://ababsurdo.fr
%
% Publié sous licence Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)
% http://creativecommons.org/licenses/by-sa/4.0/deed.fr
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Pour compiler :
%$ lualatex $basename

\documentclass[12pt]{article}

\usepackage{1920-pablo}
\usepackage{1920-pablo-cours}
\usepackage{1920-pablo-paternault}
\usepackage[]{geometry}

\begin{document}

\section{Équations cartésiennes}

\subsection{Cercle}

\begin{propriete}
  Soit $[AB]$ un segment. Le cercle de diamètre $[AB]$ est l'ensemble des points $M$ du plan vérifiant les expressions suivantes, équivalentes :
  \begin{enumerate}[(i)]
    \item $\vecteur{AM}.\vecteur{BM}=0$ ;
    \item $(x-x_A)(x-x_B)+(y-y_A)(y-y_B)=0$ (où $(x,y)$, $(x_A,y_A)$, $(x_B,y_B)$ sont les coordonnées respectives de $M$, $A$ et $B$).
  \end{enumerate}
\end{propriete}

\begin{propriete}
  Le cercle de centre $A$ et de rayon $r$ est l'ensemble des points $M$ du plan vérifiant l'une des expressions suivantes, équivalentes :
  \begin{enumerate}[(i)]
    \item $AM=r$
    \item $(x-x_A)^2+(y-y_A)^2=r^2$ (où $(x, y)$ et $(x_A, y_A)$ sont les coordonnées respectives de $M$ et $A$).
  \end{enumerate}
\end{propriete}

\subsection{Droite}

\begin{definition}
  Un \emph{vecteur normal} à une droite est un vecteur orthogonal à la direction de la droite.
\end{definition}

\begin{propriete}
  Soit une droite du plan d'équation cartésienne $ax+by+c=0$. Alors le vecteur de coordonnées $(a, b)$ est normal à cette droite.
\end{propriete}

\begin{demo}
  On sait que $\vecteur{u}(-b,a)$ est un vecteur directeur de cette droite. Soit $\vecteur v$ le vecteur de coordonnées $(a,b)$. Alors $\vecteur u.\vecteur v=-b\times a+a\times b=0$. Donc $\vecteur u$ et $\vecteur v$ sont orthogonaux, donc $\vecteur v$ est un vecteur normal à la droite.
\end{demo}

\begin{propriete}
  Soient $A$ et $\vecteur u$ un point et un vecteur du plan. La droite de vecteur normal $\vecteur u$ passant par $A$ est l'ensemble des points vérifiant $\vecteur{AM}.\vecteur{u}=0$.
\end{propriete}

\section{Trigonométrie}

\begin{multicols}{2}

  \begin{propriete}Soient $a$ et $b$ deux nombres réels.
    \begin{itemize}[$\bullet$]
      \item $\cos(a+b)=\cos a \cos b-\sin a \sin b$
      \item $\cos(a-b)=\cos a \cos b+\sin a \sin b$
      \item $\sin(a+b)=\sin a \cos b+\sin b \cos a$
      \item $\sin(a-b)=\sin a \cos b-\sin b \cos a$
    \end{itemize}
  \end{propriete}

  \begin{center}
  \begin{tikzpicture}[scale=1.5]
    \draw (0,0) circle (1);
    \draw (-1.1,0) -- (1.1,0);
    \draw (0,-1.1) -- (0,1.1);
    \draw (1,0) node[below right]{$I$};
    \draw (0,1) node[above right]{$J$};
    \draw (0,0) node[below left]{$O$};

    \draw (0,0) -- ({cos(130)}, {sin(130)}) node[above left]{$A$};
    \draw (0,0) -- ({cos(30)}, {sin(30)}) node[above right]{$B$};
    \draw[-latex] (0.6,0) arc (0:130:0.6) node at ({0.7*cos(120)},{0.7*sin(120)})[above]{$a$};
    \draw[-latex] (0.7,0) arc (0:30:0.7) node at ({0.7*cos(15)},{0.7*sin(15)})[right]{$b$};
  \end{tikzpicture}
\end{center}

\end{multicols}

\begin{demo}

  Soient $a$ et $b$ deux réels, et $A$ et $B$ deux points tels que
  $(\vecteur\imath, \vecteur{OA})=a$ et $(\vecteur\imath, \vecteur{OB})=b$. Alors les coordonnées de $A$ et $B$ dans le repère $(0, \vecteur\imath, \vecteur\jmath)$ orthonormé sont respectivement $(\cos a, \sin a)$ et $(\cos b, \sin b)$.

  \begin{enumerate}
    \item Commençons par démontrer que $\cos (a-b)=\cos a \cos b+\sin a \sin b$.
      \begin{itemize}[$\bullet$]
        \item Supposons que $0\leq b\leq a<2\pi$. Exprimons le produit scalaire $\vecteur{OB}.\vecteur{OA}$ de deux manières différentes.
          \begin{itemize}
            \item D'une part, $\vecteur{OB}.\vecteur{OA}=\cos a \cos b + \sin a \sin b$.
            \item D'autre part, $\vecteur{OB}.\vecteur{OA}=OB.OA.\cos\left(\vecteur{OB};\vecteur{OA}\right)$. Or, on a :
\begin{align*}
\left(\vecteur{OB};\vecteur{OA}\right) &=
\left(\vecteur{OB};\vecteur{\imath}\right) + \left(\vecteur{\imath};\vecteur{OA}\right) \\
&= -\left(\vecteur{\imath};\vecteur{OB}\right) + \left(\vecteur{\imath};\vecteur{OA}\right)\\
&= -b+a\\
&= a-b
\end{align*}
Donc $\vecteur{OB}.\vecteur{OA}=\cos a-b$.
          \end{itemize}
          Donc $\cos a\cos b+\sin a\sin b=\cos (a-b)$.
        \item Supposons maintenant que $0\leq a\leq b<2\pi$. Avec le même raisonnement, nous trouvons que $\cos a\cos b+\sin a\sin b=\cos(a-b)$. Or $\cos(a-b)=\cos-(a-b)=\cos(b-a)$, donc nous retrouvons la formule précédente.
        \item Enfin, les cas où $a\notin [0;2\pi[$ ou $b\notin[0;2\pi[$ se déduisent des précédents en utilisant le fait que pour tout réel $t$, $\cos (t+2k\pi)=\cos t$ et $\sin(t+2k\pi)=\sin t$ (pour $k\in\mathbb{Z}$).
      \end{itemize}

    \item $\cos(a+b)=\cos(a-(-b))=\cos a\cos(-b)+\sin a\sin(-b)=\cos a\cos b-\sin a\sin b$.
    \item $\sin(a+b)=\cos(\frac{\pi}{2}-(a+b))=\cos((\frac{\pi}{2}-a)-b)$.

      Donc $\sin(a+b)=\cos(\frac{\pi}{2}-a)\cos b+\sin(\frac{\pi}{2}-a)\sin b=\sin a\cos b+\cos a\sin b$.
    \item $\sin(a-b)=\sin(a+(-b))=\sin a\cos(-b)+\cos a\sin(-b)=\sin a\cos b-\cos a\sin b$.
  \end{enumerate}

\end{demo}

\begin{corollaire}[Duplication]Soit $a$ un réel.
  \begin{multicols}{2}
    \begin{itemize}[$\bullet$]
      \item $\sin 2a=2\sin a\cos a$
      \item $\cos 2a=\cos^2a-\sin^2a=1-2\sin^2a=2\cos^2a-1$
      \item $\cos^2a=\frac{1+\cos2a}{2}$
      \item $\sin^2a=\frac{1-\cos2a}{2}$
    \end{itemize}
  \end{multicols}
\end{corollaire}
\begin{demo}
  Ce sont des applications de la propriété précédente, en prenant $b=a$, en considérant également l'égalité $\cos^2a+\sin^2a=1$.
\end{demo}

\end{document}
