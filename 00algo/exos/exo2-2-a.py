def f(x):
    return x**2-3*x+3

x = -10
minimum = f(-10)
x_min = -10
while x <= 10:
    if f(x) < minimum:
        x_min = x
        minimum = f(x)
    x = x + .5
print x_min, minimum

