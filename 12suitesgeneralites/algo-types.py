#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
# Version 2, December 2004
#
# Copyright (C) 2014 Louis Paternault
#
# Everyone is permitted to copy and distribute verbatim or modified
# copies of this license document, and changing it is allowed as long
# as the name is changed.
#
# DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
# TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
#
# 0. You just DO WHAT THE FUCK YOU WANT TO.

"""Algorithmes types de manipulation de suites

La suite manipulée est la suite arithmétique de premier terme 7 et de raison 3.

# Tests

python3 algo-types.py

"""

U0 = 7
RAISON = 3

import unittest

def terme(n):
    """Renvoie le terme de rang n de la suite arithmétique considérée.

    Le premier terme est u_0.
    """
    k = 0
    u = U0
    while k < n:
        k = k + 1
        u = u + RAISON
    return u

def seuil(seuil):
    """Renvoie l'indice et la valeur du premier terme de la suite dépassant seuil.
    """
    k = 0
    u = U0
    while u < seuil:
        k = k + 1
        u = u + RAISON
    return (k, u)

def somme(n):
    """Renvoie la somme des n premiers termes de la suite."""
    k = 1
    u = U0
    somme = U0
    while k < n:
        k = k + 1
        u = u + RAISON
        somme = somme + u
    return somme

def seuil_somme(seuil):
    """Renvoie la plus petite somme de la suite dépassant seuil."""
    k = 1
    u = U0
    somme = U0
    while somme < seuil:
        k = k + 1
        u = u + RAISON
        somme = somme + u
    return (k, somme)

# pylint: disable=too-many-public-methods
class Tests(unittest.TestCase):
    """Teste les fonctions."""

    def test_terme(self):
        """Teste la fonction terme()
        """
        self.assertEqual(terme(0), 7)
        self.assertEqual(terme(1), 10)
        for n in range(1, 10):
            self.assertEqual(terme(n), 7 + n * 3)

    def test_seuil(self):
        """Teste la fonction seuil()
        """
        self.assertEqual(seuil(6), (0, 7))
        self.assertEqual(seuil(10), (1, 10))
        self.assertEqual(seuil(12), (2, 13))

    def test_somme(self):
        """Teste la fonction somme()
        """
        self.assertEqual(somme(1), 7)
        self.assertEqual(somme(2), 17)
        for n in range(1, 10):
            self.assertEqual(somme(n), n * (terme(0) + terme(n-1))/2)

    def test_seuil_somme(self):
        """Teste la fonction seuil_somme
        """
        self.assertEqual(seuil_somme(0), (1, 7))
        self.assertEqual(seuil_somme(7), (1, 7))
        self.assertEqual(seuil_somme(17), (2, 17))
        self.assertEqual(seuil_somme(12), (2, 17))

if __name__ == '__main__':
    unittest.main()
