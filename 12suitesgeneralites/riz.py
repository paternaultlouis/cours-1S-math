#!/usr/bin/env python3

n = int(input("n = ? "))
case = 1
somme = 0

for k in range(n):
    case = 2*case
    somme = somme + case

print(somme)
