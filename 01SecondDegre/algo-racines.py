# On importe la bibliothèque math, pour que Python puisse calculer des racines carrées
from math import sqrt

# On demande à l'utilisateur la valeur des trois paramètres a, b et c
a = float(input("a=? "))
b = float(input("b=? "))
c = float(input("c=? "))

# Calcul du discriminant
delta = b**2-4*a*c

# Trois cas, selon la valeur du discriminant
if delta > 0:
    print("Le trinôme a deux racines :")
    print((-b-sqrt(delta))/(2*a))
    print((-b+sqrt(delta))/(2*a))
elif delta == 0:
    print("Le trinôme a une racine double :")
    print(-b/(2*a))
else:
    print("Le trinôme n'a pas de racines.")
    
