#!/usr/bin/env python3
# -*- coding: utf-8 -*-

def forcebrute(*points):
    """Cassage par force brute du code utilisant SSSS avec les arguments.

    Entrée : Une liste de points du plan.
    Sortie : La liste des triplets (a, b, c), où a, b et c sont des chiffres
        (entiers entre 0 et 9), tels que tous les points donnés en argument
        font partie de la courbe représentative du polynôme x->ax^2+bx+c.
    """
    solutions = []
    for a in range(0, 10):
        for b in range(0, 10):
            for c in range(0, 10):
                if all([a*x*x + b*x + c == y for (x, y) in points]):
                    solutions.append((a, b, c))
    return solutions

if __name__ == "__main__":
    #print("Cassage de (-3, 12), (-2, 9)...")
    #print(forcebrute((-3, 12), (-2, 9)))

    points = [(2,28), (-1, 1)]
    print("Cassage de " + ", ".join(["({},{})".format(*p) for p in points]))
    print(forcebrute(*points))
