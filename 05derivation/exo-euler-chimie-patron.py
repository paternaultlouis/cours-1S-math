# -*- coding: utf-8 -*-

# Définition des trois courbes
import turtle
turtle.speed(0)
turtle.setworldcoordinates(0, 0, 50, 1)
ta = turtle.Turtle()
tb = turtle.Turtle()
tc = turtle.Turtle()
ta.pencolor("red")
tb.pencolor("green")
tc.pencolor("blue")

# Constantes
k1 = ???
k2 = ???
k3 = ???

# Pas
h = .1

# Valeurs initiales des trois quantités, et de leurs dérivées
a = ???
b = ???
c = ???
ap = ???
bp = ???
cp = ???


t = 0
while True:
    # Le temps passe...
    t = t + h

    # Calcul des dérivées
    ap = ???
    bp = ???
    cp = ???

    # Calcul des nouvelles quantitiés
    a = ???
    b = ???
    c = ???

    # Tracé des courbes
    ta.goto(t, a)
    tb.goto(t, b)
    tc.goto(t, c)
