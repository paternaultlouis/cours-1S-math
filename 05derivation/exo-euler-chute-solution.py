# -*- coding: utf-8 -*-

# Définition de l'interface graphique
import turtle
turtle.speed(0)
turtle.setworldcoordinates(0, 0, 50, 10)

# Valeurs initiales des coordonnées, et de leurs dérivées
x = 0
y = 0
vx = 10
vy = 10
ax = 0
ay = -9.81

# Pas
h = .05

t = 0
while True:
    # Calcul des nouvelles positions
    x = x + h * vx
    y = y + h * vy

    # Calcul des nouvelles vitesses
    vx = vx + h * ax
    vy = vy + h * ay

    # Affichage
    turtle.goto(x, y)
