# -*- coding: utf-8 -*-

# Définition de l'interface graphique
import turtle
turtle.speed(0)
turtle.setworldcoordinates(0, 0, 50, 10)

# Valeurs initiales des coordonnées, et de leurs dérivées
x = ???
y = ???
vx = ???
vy = ???
ax = ???
ay = ???

# Pas
h = .05

t = 0
while True:
    # Calcul des nouvelles positions
    x = ???
    y = ???

    # Calcul des nouvelles vitesses
    vx = ???
    vy = ???

    # Affichage
    turtle.goto(x, y)
