%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2015 Louis Paternault --- http://ababsurdo.fr
%
% Publié sous licence Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)
% http://creativecommons.org/licenses/by-sa/4.0/deed.fr
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Pour compiler :
%$ lualatex $basename
%$ lualatex $basename

\documentclass[12pt]{article}

\usepackage{1920-pablo}
\usepackage{1920-pablo-devoir}
\usepackage{1920-pablo-paternault}
\usepackage[a5paper, margin=1.0cm]{geometry}

\title{Méthode d'Euler}
\classe{}
\dsnum{}

\begin{document}

\maketitle

On appelle \emph{équation différentielle} une équation mettant en œuvre une ou plusieurs fonctions inconnues et leurs dérivées. Par exemple, trouver les fonctions $f$ telles que $f'\left( x \right)+2f\left( x \right)=0$. Ce genre d'équations sera étudié l'an prochain.

La \emph{méthode d'Euler} permet de trouver une solution approchée de certaines de ces équations.

\begin{exercice}[Expérimentations, et Calculs théoriques]~
  \begin{center}
    \begin{tikzpicture}[ultra thick, scale=1]
      \begin{scope}
        \clip (-1, 0) rectangle (5, 7);
        \draw[gray!50!white, thin, xshift=-0.5cm, yshift=-0.5cm] (-1, -1) grid (6, 8);
        \draw[gray, thin] (-1, 0) grid (5, 7);
      \end{scope}
      \draw[-latex] (-1, 0) -- (5, 0);
      \draw[-latex] (0, 0) -- (0, 7);
      \foreach \x in {-1, 0, ..., 5} {
        \draw (\x, 0) node[below, fill=white, opacity=.7, text opacity=1]{$\x$};
      }
      \foreach \y in {1, 2, ..., 7} {
        \draw (0, \y) node[left, fill=white, opacity=.7, text opacity=1]{$\y$};
      }

      \draw[domain=-1:5,smooth,variable=\x,blue] plot ({\x},{\x*\x/4});

    %\draw[dashed] (2, 0) node[below]{$x$} -- (2, {2*2/4}) node{$\bullet$} -- (0, {2*2/4}) node[left]{$f\left( x \right)$};
    %\draw[dashed] (3, 0) node[below]{$x+h$} -- (3, {3*3/4}) node{$\bullet$} -- (0, {3*3/4}) node[left]{$f\left( x +h\right)$};
    %\draw[latex-latex] (2, -1) -- (3, -1) node[below, midway]{$h$};
    %\draw[dotted] (1, 0) -- (4, 3);
    \end{tikzpicture}
  \end{center}

  \begin{enumerate}
    \item On a tracé ci-dessus la courbe $\mathcal{C}_f$ de la fonction $f:x\mapsto \frac{x^2}{4}$.
      \begin{enumerate}
        \item Déterminer (par le calcul) l'équation de la tangente à $\mathcal{C}_f$ au point d'abscisse 2. On appelle $d$ la fonction affine correspondante.
        \item Tracer cette tangente.
        \item Calculer $f(x)-d(x)$ pour $x=4$, $x=3$, $x=2,5$, et $x=2,1$.
        \item Vers quelle valeur semble tendre $f\left( x \right)-d\left( x \right)$ lorsque $x$ tend vers 2 ?
      \end{enumerate}
    \item On cherche à résoudre l'équation différentielle :
      \[\left\{\begin{array}{l}
          2f\left( x \right)-xf'\left( x \right)=0\\
          f\left( 2 \right) = 1 \\
      \end{array}\right.\]
      Dans les questions \ref{q:euler:debut} à \ref{q:euler:fin}, on suppose que le tracé correspond \emph{exactement} au tracé de la courbe de la fonction $f$.
      \begin{enumerate}
        \item Isoler $f'\left( x \right)$ dans l'équation précédente.
        \item\label{q:euler:debut} Placer le point de coordonnées $\left( 2; f\left( 2 \right) \right)$.
        \item Calculer $f'\left( 2 \right)$, puis tracer le segment de la droite passant par le point de coordonnées $\left( 2; f\left( 2 \right) \right)$, et de coefficient directeur $f'(2)$, pour $x\in\left[ 2; 3 \right]$.
        \item\label{q:euler:fin} Même question, pour une abscisse de 3, puis de 4.
        \item Vérifier que la fonction $f:x\mapsto\frac{x^2}{4}$ est une solution de l'équation.
        \item Comparer la courbe tracée à cette question, et la courbe de la fonction $f$. Comment faire pour améliorer la précision du tracé ?
          % TODO Faire tracer cette courbe sur un autre graphique, et comparer ensuite les deux courbes
      \end{enumerate}
    \item \emph{Généralisation} Soit $f$ une fonction dérivable, et $a$ un réel de son ensemble de définition.
      On suppose que pour des valeurs de $h$ suffisament petites, $f\left( a+h \right)$ est exactement égale à l'image de $a+h$ par la tangente à $f$ en $a$.
      \begin{enumerate}
        \item Rappeler l'équation de la tangente à $f$ au point d'abscisse $a$.
        \item En déduire l'expression de $f\left( a+h \right)$ en fonction de $f\left( a \right)$, $f'\left( a \right)$ et $h$.
      \end{enumerate}
  \end{enumerate}
\end{exercice}

\pagebreak

\begin{exercice}[Mise en œuvre informatique]~
  \begin{enumerate}
    \item\label{q:chimie}\emph{Cinétique chimique} La réaction de l'oxydation du sulfite de cuivre peut être modélisée par les équations suivantes.
      \[\left\{\begin{array}{rclp{9em}}
          A &\longrightarrow& B & Réaction lente\\
          B + B &\longrightarrow& C + B & Réaction très rapide \\
          B + C &\longrightarrow& A + C & Réaction rapide
      \end{array}\right.\]

      On appelle $a$, $b$ et $c$ les quantités (en unité arbitraire) des composants $A$, $B$ et $C$ au cours du temps : $a\left( 0 \right)$ est la quantité de $A$ initiale ; $b\left( 3 \right)$ est la quantité de $B$ au bout de trois secondes, etc. Ces fonctions $a$, $b$, $c$ vérifient le système d'équations différentielles suivant (avec $k_1=0,04$, $k_2=3$, $k_3=0,1$) :

      \[\left\{\begin{array}{rcrccccc}
          \multicolumn{8}{p{16em}}{$a\left( 0 \right)=1$ ; $b\left( 0 \right)=0$ ; $c\left( 0 \right)=0$} \\
          a'\left( t \right) &=& -&k_1a\left( t \right)&+&k_3b\left( t \right)c\left( t \right)\\
          b'\left( t \right) &=&  &k_1a\left( t \right)&-&k_3b\left( t \right)c\left( t \right)&-&k_2b\left( t \right)^2\\
          c'\left( t \right) &=&  &                    & &                                     & &k_2b\left( t \right)^2\\
      \end{array}\right.\]

      En utilisant, au choix, un tableur (LibreOffice Calc) ou le langage Python, et avec la méthode d'Euler, tracer la représentation graphique des fonctions $a$, $b$, $c$.
      
      % TODO Donner les formules initiales, pour A, de A' et A(n+1)
      % TODO Corriger les dérivées initiales, qui ne sont pas nulles

    \item\label{q:chute}\emph{Chute libre} En mécanique, deuxième loi de Newton (ou « principe fondamental de la dynamique ») donne la relation entre l'accélération d'un corps de masse $m$ et les forces extérieures s'appliquant à ce corps :
      \[\displaystyle\sum\vecteur{F}=m\vecteur{a}\]

      Un mobile en chute libre n'est soumis qu'à la gravité, qui s'exprime comme $m\vecteur{g}$ (où $\vecteur{g}$ est un vecteur vertical, orienté vers le bas, de norme $g$, intensité de la pesanteur).

      Par définition, l'accélération $\vecteur{a}$ d'un mobile est égale à la dérivée de sa vitesse $\vecteur{v}$, elle même égale à la dérivée de sa position $\vecteur{r}$. L'équation différentielle vérifiée par la position $\vecteur{r}$ est donc :
      $m\frac{d^2\vecteur{r}}{dt^2}=-mg\vecteur{z}$ (où $\frac{d^2f}{dt^2}$ est la notation « physicienne » pour $f''(t)$, la dérivée seconde de $f$). Cela donne, en décomposant selon deux coordonnées (on se place dans un plan vertical) :
      \[\left\{\begin{array}{rcl}
          \multicolumn{3}{p{16em}}{$x\left( 0 \right) = 0$ ; $z\left( 0 \right) = 0$} \\
          \multicolumn{3}{p{16em}}{$x'\left( 0 \right) = 10$ ; $z'\left( 0 \right) = 10$} \\
          mx''\left( t \right) &=& 0\\
          mz''\left( t \right) &=& -mg\\
      \end{array}\right.\]
      En utilisant, au choix, un tableur (LibreOffice Calc) ou le langage Python, et avec la méthode d'Euler, tracer la représentation graphique de la trajectoire du mobile.
    \item Répondre aux questions suivantes, dans l'ordre de votre choix.
      \begin{enumerate}
        \item Dans la question \ref{q:chimie} (cinétique chimique), les valeurs de $k_1$, $k_2$ et $k_3$ ont été altérée pour faciliter la résolution. Refaire l'exercice avec les vraies valeurs $k_1 = 0,04$, $k_2 = 3\times10^7$, $k_3 = 10^4$. Un problème devrait se produire : l'identifier et le résoudre.
        \item Dans la question \ref{q:chute} (chute libre), ajouter un des paramètres suivants :
          \begin{enumerate*}[(i)]
          \item le rebond du mobile, quand il atteint le sol en $z=0$ ;
          \item la résistance de l'air (ou le vent), dont il faudra déterminer une expression, qui applique une force horizontale à l'objet.
          \end{enumerate*}
        \item Utiliser cette méthode pour résoudre un autre problème, par exemple un des problèmes suivants. Pour chacun, il faudra déterminer (en étudiant le problème, ou par une recherche sur internet) une équation différentielle modélisant le problème, et appliquer la méthode d'Euler pour le résoudre.
          \begin{enumerate}
            \item Mouvement d'une masse accrochée à un ressort. Dans un premier temps, on pourra ignorer le frottement, puis le rajouter, puis considérer un mouvement entretenu (ce qui est le cas pour un séisme). Voir par exemple : \url{http://fr.wikipedia.org/wiki/Système_masse-ressort}.
            \item Étude de la taille des populations d'une proie et d'un prédateur (lions et antilopes, requins et sardines, trolls et gobelins, etc.). Voir par exemple : \url{https://fr.wikipedia.org/wiki/Équations_de_Lotka-Volterra}.
            \item Un autre problème de votre choix.
          \end{enumerate}
      \end{enumerate}
  \end{enumerate}
\end{exercice}

\end{document}
