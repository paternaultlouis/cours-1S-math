%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2016 Louis Paternault --- http://ababsurdo.fr
%
% Publié sous licence Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)
% http://creativecommons.org/licenses/by-sa/4.0/deed.fr
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Pour compiler :
%$ lualatex $basename

\documentclass[12pt]{article}

\usepackage{1920-pablo}
\usepackage{1920-pablo-cours}
\usepackage{1920-pablo-paternault}
\usepackage[]{geometry}

\begin{document}

\section{Nombre dérivé d'une fonction}

\begin{definition}[Taux d'accroissement]
  Soit $f$ une fonction définie sur $\cal D$, et $a\in{\cal D}$. Pour $h\neq0$, $a+h\in{\cal D}$, on appelle taux d'accroissement le rapport :
  \[\frac{f(a+h)-f(a)}{h}\]
\end{definition}

\begin{remarque}
  Le taux d'accroissement est la pente de la droite passant par les points
  $\left(a, f(a)\right)$ et $\left(a+h, f(a+h)\right)$.
\begin{center}
    \begin{tikzpicture}[very thick]
      \draw[dotted,color=gray] (-.5,0) grid (4,4);
      \draw[] (-.5,0) -- (4,0);
      \draw[] (0,0) -- (0,4);
      \draw[->] (0,0) -- (1,0) node[midway,below]{$\vecteur\imath$};
      \draw[->] (0,0) -- (0,1) node[midway,above left]{$\vecteur\jmath$};
      \draw[domain=-.5:4,smooth,blue] plot ({\x},{pow(\x-1.5, 2)/2+.5});
      \draw[dashed] (1.3, 0) node[below]{$a$} -- ++(0, {pow(1.3-1.5, 2)/2+.5}) -- ++(-1.3, 0) node[left]{$f(a)$};
      \draw[dashed] (3.4, 0) node[below]{$a+h$} -- ++(0, {pow(3.4-1.5, 2)/2+.5}) -- ++(-3.4, 0) node[left]{$f(a+h)$};
      \draw[dotted, red] (1.3, {pow(1.3-1.5, 2)/2+.5}) -- ++({3.4-1.3}, 0) node[below, midway]{$h$} -- (3.4, {pow(3.4-1.5, 2)/2+.5}) node[right, midway]{$f\left( a+h \right)-f\left( a \right)$} -- cycle;
    \end{tikzpicture}
  \end{center}
\end{remarque}

\begin{definition}[Nombre dérivé]
  Soit $f$ une fonction définie sur $\cal D$, et $a\in{\cal D}$. S'il existe $l$ tel que la limite de $\frac{f(a+h)-f(a)}{h}$ tende vers $l$ quand $h$ tend vers $0$, alors :
  \begin{itemize}
    \item on dit que $f$ est\emph{dérivable} en $a$ ;
    \item on note $f'(a)=\lim_{h\rightarrow0}\frac{f(a+x)-f(a)}{h}=l$ ;
    \item $l=f'(a)$ est appelé \emph{nombre dérivé de $f$ en $a$}.
  \end{itemize}
\end{definition}

\begin{propriete}[Interprétation géométrique]
  Si une fonction $f$ est dérivable en $a$, alors $f'(a)$ est la pente de la
  tangente à la courbe de $f$ en $(a, f(a))$.
\end{propriete}

\section{Tangente}

\begin{definition}[Tangente]
  Soient $I$ un intervalle, $a\in I$, $f$ une fonction définie sur $I$,
  dérivables en $a$, et ${\cal C}_f$ la courbe représentative de $f$. La droite
  passant par le point $(a, f(a))$ et de coefficient directeur $f'(a)$ est
  appelée \emph{tangente à ${\cal C}_f$ au point d'abscisse $a$}.
\end{definition}

\begin{remarque}
  Visuellement, cette définition correspond à la définition déjà connue d'une
  tangente à un cercle : la tangente à une courbe ${\cal C}_f$ au point de
  coordonnées $(a, f(a))$ est la droite passant par ce point, et ne « touchant
  » la courbe qu'en ce point là, dans un voisinage de ce point.
\end{remarque}

\begin{propriete}[Tangente]
  Soit $f$ une fonction dérivable en $a$. L'équation de la tangente à la courbe de $f$ en $(a, f(a))$ est :
  \[y=f'(a)(x-a)+f(a)\]
\end{propriete}

\section{Fonctions et opérations usuelles}

\begin{definition}[Dérivée d'une fonction]
  Soit $f$ une fonction définie sur $\cal D$, et $I$ un sous-ensemble de $\cal
  D$. On dit que $f$ est \emph{dérivable sur $I$} si $f$ est dérivable en tout
  point de $I$.

  On appelle \emph{fonction dérivée de $f$}, et on note $f'$, la fonction qui à
  tout point $x$ de $I$ associe le réel $f'(x)$, nombre dérivé de $f$ en $x$.
\end{definition}


\begin{propriete}[Dérivées des fonctions usuelles]~
  \begin{itemize}
    \item Sur $\left] 0; +\infty \right[$, la dérivée de la fonction racine $x\mapsto\sqrt x$ est $x\mapsto\frac{1}{2\sqrt{x}}$.
      \item Sur $\mathbb{R}^*$, la dérivée de la fonction inverse $x\mapsto\frac{1}{x}$ est $x\mapsto-\frac{1}{x^2}$.
      \item Sur $\mathbb{R}$, pour $n$ un entier naturel non nul, la dérivée de $x\mapsto x^n$ est $x\mapsto nx^{n-1}$.
  \end{itemize}
\end{propriete}

\begin{remarque}La dernière propriété est vraie pour tout $\alpha$ réel non nul, pour $x\in\mathbb{R}^{+*}$ (ou $x\in\mathbb{R}^*$ is $\alpha>0$) : la
  dérivée de $x\mapsto x^\alpha$ est $x\mapsto \alpha x^{\alpha-1}$. De là, on peut en déduire les deux propriétés précédentes : soient $u:x\mapsto\sqrt{x}$ et $v:x\mapsto\frac{1}{x}$. Alors, quel que soit $x$ réel, $u(x)=\sqrt{x}=x^\frac{1}{2}$ et $v(x)=\frac{1}{x}=x^{-1}$, et :

  \[u'(x) = \frac{1}{2}x^{\frac{1}{2}-1}=\frac{1}{2}x^{-\frac{1}{2}}=\frac{1}{2}\frac{1}{x^\frac{1}{2}}=\frac{1}{2\sqrt{x}}\]
  \[v'(x) = -1\times x^{-1-1}=-x^{-2}=-\frac{1}{x^2}\]
\end{remarque}

\begin{corollaire}Sur $\mathbb{R}$ :
  \begin{itemize}
    \item la dérivée d'une fonction constante (pour un $k$ réel) $x\mapsto k$ est la fonction nulle $x\mapsto 0$ ;
    \item la dérivée d'une fonction affine $x\mapsto ax+b$ est une fonction constante $x\mapsto a$ ;
    \item la dérivée de la fonction carrée $x\mapsto x^2$ est $x\mapsto 2x$.
  \end{itemize}
\end{corollaire}

\begin{propriete}[Opérations usuelles] Soient $u$, $v$ deux fonctions dérivables sur un intervalle $I$, et $\lambda$ un réel. Alors :
  \begin{itemize}
    \item $(\lambda u)'=\lambda u'$ ;
    \item $(u+v)' = u'+v'$ (la dérivée de la somme est la somme des dérivée) ;
    \item $(u\times v)' = u'v + v'u$ ;
    \item $\left(\frac{u}{v}\right)'= \frac{u'v-v'u}{v^2}$ (si $v$ ne s'annule pas sur $I$).
  \end{itemize}
\end{propriete}


\end{document}
