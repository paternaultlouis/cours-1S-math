# -*- coding: utf-8 -*-

# Définition des trois courbes
import turtle
turtle.speed(0)
turtle.setworldcoordinates(0, 0, 50, 1)
ta = turtle.Turtle()
tb = turtle.Turtle()
tc = turtle.Turtle()
ta.pencolor("red")
tb.pencolor("green")
tc.pencolor("blue")

# Constantes
k1 = 0.04
k2 = 3
#k2 = 3*10**7
k3 = .1
#k3 = 10**4

# Pas
h = .1

# Valeurs initiales des trois quantités, et de leurs dérivées
a = 1
b = 0
c = 0
ap = 0
bp = 0
cp = 0


t = 0
while True:
    # Le temps passe...
    t = t + h

    # Calcul des dérivées
    ap = -k1 * a + k3 * b * c
    bp = k1 * a - k3 * b * c - k2 * b**2
    cp = k2 * b**2

    # Calcul des nouvelles quantitiés
    a = a + ap * h
    b = b + bp * h
    c = c + cp * h

    # Tracé des courbes
    ta.goto(t, a)
    tb.goto(t, b)
    tc.goto(t, c)
