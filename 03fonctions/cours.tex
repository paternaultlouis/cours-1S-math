%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2017 Louis Paternault --- http://ababsurdo.fr
%
% Publié sous licence Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)
% http://creativecommons.org/licenses/by-sa/4.0/deed.fr
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Pour compiler :
%$ lualatex $basename

\documentclass[12pt]{article}

\usepackage{1920-pablo}
\usepackage{1920-pablo-cours}
\usepackage{1920-pablo-paternault}
\usepackage[]{geometry}

\begin{document}

\section{Variations}

\begin{definition}
    Soit une fonction $f:\mathcal{D}\rightarrow\mathbb{R}$ (où $\mathcal{D}\subset\mathbb{R}$).
  \begin{itemize}
    \item On dit que $f$ est \emph{croissante} (respectivement \emph{strictement croissante}) si quels que soient $a$ et $b$ dans $\mathcal{D}$, si $a<b$, alors $f(a)\leq f(b)$ (respectivement $f(a)<f(b)$). On dit aussi que « la fonction $f$ conserve l'ordre ».
    \item On dit que $f$ est \emph{décroissante} (respectivement \emph{strictement décroissante}) si quels que soient $a$ et $b$ dans $\mathcal{D}$, si $a<b$, alors $f(a)\geq f(b)$ (respectivement $f(a)>f(b)$). On dit aussi que « la fonction $f$ inverse l'ordre ».
\item On dit que $f$ est \emph{constante} si quels que soient $a$ et $b$ dans $\mathcal{D}$, alors $f(a)=f(b)$.
    \item On dit qu'une fonction est \emph{monotone} (respectivement \emph{strictement monotone}) si elle est croissante ou décroissante (respectivement strictement croissante ou décroissante).
  \end{itemize}
\end{definition}

\begin{center}
    \begin{tikzpicture}[very thick]
      \draw[dotted,color=gray] (-.5,0) grid (4,4);
      \draw[] (-.5,0) -- (4,0);
      \draw[] (0,0) -- (0,4);
      \draw[->] (0,0) -- (1,0) node[midway,below]{$\vecteur\imath$};
      \draw[->] (0,0) -- (0,1) node[midway,left]{$\vecteur\jmath$};
      \draw[domain=-.5:4,smooth,blue] plot ({\x},{pow(\x+1, 2)/8+1});
      \draw[dashed] (1.7, 0) node[below]{$a$} -- ++(0, {pow(1.7+1, 2)/8+1}) -- ++(-1.7, 0) node[left]{$f(a)$};
      \draw[dashed] (2.4, 0) node[below]{$b$} -- ++(0, {pow(2.4+1, 2)/8+1}) -- ++(-2.4, 0) node[left]{$f(b)$};
    \end{tikzpicture}
    ~
    \begin{tikzpicture}[very thick]
      \draw[dotted,color=gray] (-.5,0) grid (4,4);
      \draw[] (-.5,0) -- (4,0);
      \draw[] (0,0) -- (0,4);
      \draw[->] (0,0) -- (1,0) node[midway,below]{$\vecteur\imath$};
      \draw[->] (0,0) -- (0,1) node[midway,left]{$\vecteur\jmath$};
      \draw[domain=-.5:4,smooth,blue] plot ({\x},{4-pow(\x+1, 2)/8});
      \draw[dashed] (1.7, 0) node[below]{$a$} -- ++(0, {4-pow(1.7+1, 2)/8}) -- ++(-1.7, 0) node[left]{$f(a)$};
      \draw[dashed] (2.4, 0) node[below]{$b$} -- ++(0, {4-pow(2.4+1, 2)/8}) -- ++(-2.4, 0) node[left]{$f(b)$};
    \end{tikzpicture}
  \end{center}

\section{Fonctions associées}

\begin{propriete}[Fonctions associées]
Soit $u$ une fonction définie sur un intervalle $I$ de $\mathbb R$, et ${\cal
C}_u$ sa courbe représentative.

\begin{enumerate}[(a)]
  \item Étant donné un réel $k$, la courbe de la fontion $u+k$ (c'est-à-dire
    la fonction $x\mapsto u(x)+k$) est l'image de la courbe ${\cal C}_u$ par
    la translation de vecteur $(0,k)$.

    De plus, les fonctions $u$ et $u+k$ ont les mêmes variations sur $I$.

  \item Étant donné un réel strictement positif $\lambda$, la courbe de la
    fonction $\lambda u$ (c'est-à-dire la fonction $x\mapsto \lambda u(x)$) est
    l'image de ${\cal C}_u$ par une « compression ou un étirement vertical ».
    Si $\lambda$ est négatif, la courbe de $\lambda u$ est l'image de ${\cal
    C}_u$ par une « compression ou un étirement vertical », suivie d'une symétrie par rapport à l'axe des abscisses.

    De plus :
    \begin{itemize}
      \item si $\lambda$ est positif, les fonctions $u$ et $\lambda u$
        ont mêmes variations sur $I$ ;
      \item si $\lambda$ est négatif, les fonctions $u$ et $\lambda u$
        ont des variations opposées sur $I$.
    \end{itemize}
  \item La fonction $u$ étant supposée positive sur $I$, la fonction $\sqrt{u}$
    a les mêmes variations que la fonction $u$ sur $I$.
  \item La fonction $u$ ne s'annulant pas sur $I$, la fonction $\frac{1}{u}$
    a des variations opposées à celles de $u$ sur $I$.
\end{enumerate}
\end{propriete}

\begin{demo} Soit $u$ une fonction croissante sur un intervalle $I$ de $\mathbb{R}$ (la démonstration est similaire si $u$ est décroissante).

  \begin{enumerate}[(a)]
    \item Soit $k$ un réel quelconque, et $a$ et $b$ deux éléments de $I$, tels que $a<b$. Alors :
      \begin{align*}
        a &< b \\
        f(a) &\leq f(b) \text{ car $f$ est croissante}\\
        f(a)+k &\leq f(b)+k
      \end{align*}
      Donc la fonction $x\mapsto f(x)+k$ est croissante.
    \item Soit $\lambda$ un réel strictement positif, et $a$ et $b$ deux éléments de $I$, tels que $a<b$. Alors :
      \begin{align*}
        a &< b \\
        f(a) &\leq f(b) \text{ car $f$ est croissante} \\
        \lambda f(a) &\leq \lambda f(b) \text{ car $\lambda$ est positif} \\
      \end{align*}
      Donc la fonction $x\mapsto \lambda f(x)$ est croissante.

      De même, si $lambda$ est strictement négatif\ldots À vous !
  \end{enumerate}
\end{demo}

\begin{methode}[Application, sur un exemple] Quelles sont les variations de la fonction $f:x\mapsto\frac{1}{\sqrt{x^2+2x+5}}+7$, définie sur $\mathbb{R}$ ?

  La fonction $x\mapsto x^2+2x+5$ est un trinôme du second degré, décroissant sur $\left] -\infty; -1 \right]$, et croissant ensuite. En utilisant les fonctions associées, on obtient :

  \begin{center}
    \begin{tikzpicture}
      \tkzTabInit[lgt=3,espcl=3]
      {
        $x$ /1,
        $x^2+2x+5$ /1,
        $\sqrt{x^2+2x+5}$ /1,
        $\frac{1}{\sqrt{x^2+2x+5}}$ /1,
        $\frac{1}{\sqrt{x^2+2x+5}}+7$ /1
      }
        {$-\infty$,$-1$,$+\infty$}
        \tkzTabVar{+/, -/4, +/}
        \tkzTabVar{+/, -/2, +/}
        \tkzTabVar{-/, +/$\frac{1}{2}$, -/}
        \tkzTabVar{-/, +/$\frac{15}{2}$, -/}
    \end{tikzpicture}
  \end{center}

\end{methode}

\section{Position relative}

\begin{propriete}
  Étant donnés deux fonctions $f$ et $g$, et leurs courbes $\mathcal{C}_f$ et $\mathcal{C}_g$ respectives, «~$\mathcal{C}_f$ est en dessous de $\mathcal{C}_g$ sur l'intervalle $I$~» est équivalent à «~pour tout $x\in I$, $f(x)\leq g(x)$~».
\end{propriete}


\section{Racine carrée}

\begin{definition} ~

  \begin{itemize}
    \item Étant donné un nombre réel positif $a$, sa racine carrée $\sqrt{a}$
      désigne l'unique nombre positif dont le carré est $a$.
    \item La fonction racine carrée est la fonction définie sur ${\mathbb R}^+$ qui a chaque réel positif $x$ associe sa racine carrée $\sqrt{x}$.
  \end{itemize}
\end{definition}

\begin{center}
    \begin{tikzpicture}[very thick,scale=2]
      \draw[dotted,color=gray] (0,0) grid (4,2);
      \draw[] (-.2,0) -- (4,0);
      \draw[] (0,-.2) -- (0,2);
      \draw[->] (0,0) -- (1,0) node[midway,below]{$\vecteur\imath$};
      \draw[->] (0,0) -- (0,1) node[midway,left]{$\vecteur\jmath$};
      \draw[domain=0:4,smooth,blue] plot ({\x},{sqrt(\x)});
    \end{tikzpicture}
  \end{center}

\begin{propriete}
  La fonction racine carrée est strictement croissante sur $\mathbb R^+$.
\end{propriete}
\begin{demo}
  Soient $a$ et $b$ deux nombres de $\mathbb{R}^+$, avec $a<b$. On a :
  \begin{align*}
    a &< b \\
    a-b &< 0 \\
    \sqrt{a}^2-\sqrt{b}^2 &< 0 \text{ car $a$ et $b$ sont positifs} \\
    \left( \sqrt{a}-\sqrt{b} \right)\left( \sqrt{a}+\sqrt{b} \right) &< 0 \\
    \sqrt{a}-\sqrt{b} &< \frac{0}{\sqrt{a}+\sqrt{b}} \text{ car $\sqrt{a}+\sqrt{b}$ est strictement positif} \\
    \sqrt{a}-\sqrt{b} &< 0 \\
    \sqrt{a} &< \sqrt{b} \\
  \end{align*}

  Nous avons montré que si $a<b$, alors $\sqrt{a}<\sqrt{b}$ : donc la fonction racine est strictement croissante.
\end{demo}

\begin{propriete}[Positions relatives]
  Soient ${\cal C}_x$, ${\cal C}_{x^2}$, ${\cal C}_{\sqrt{x}}$, les courbes respectives des fonctions $x\mapsto x$, $x\mapsto x^2$, $x\mapsto \sqrt x$ définies sur $\mathbb R^+$. Alors :
  \begin{itemize}
    \item si $x\in[0;1]$, alors ${\cal C}_{x^2}$ est en dessous de ${\cal C}_x$, elle même en dessous de  ${\cal C}_{\sqrt{x}}$ ;
    \item si $x\in[1;+\infty]$, alors ${\cal C}_{x^2}$ est au dessus de ${\cal C}_x$, elle même au dessus de  ${\cal C}_{\sqrt{x}}$.
  \end{itemize}
\end{propriete}


\begin{demo}[TODO : À détailler]
  \begin{itemize}
    \item Premier cas : en 0 et 1, les courbes sont confondues, donc tout va bien.
    \item Second cas : soit $0<x<1$. Alors $\sqrt{0}<\sqrt{x}<\sqrt{1}$ (car $\sqrt{.}$ est croissante), et $0<\sqrt{x}<1$. En multipliant par $\sqrt{x}>0$, on a $0<x<\sqrt{x}$. En élevant au carré (cette fonction étant croissante sur les positifs), on a $0<x^2<x$. CQFD.
    \item Troisième cas : soit $x>1$. Idem.
  \end{itemize}
\end{demo}

\begin{center}
    \begin{tikzpicture}[domain=0:3,very thick,scale=2]
      \draw[dotted,color=gray] (0,0) grid (3,3);
      \draw[] (-.2,0) -- (3,0);
      \draw[] (0,-.2) -- (0,3);
      \draw[->] (0,0) -- (1,0) node[midway,below]{$\vecteur\imath$};
      \draw[->] (0,0) -- (0,1) node[midway,left]{$\vecteur\jmath$};
      \draw[smooth,blue] plot ({\x},{\x});
      \draw[smooth,blue] plot ({\x},{sqrt(\x)});
      \draw[smooth,blue,domain=0:{sqrt(3)}] plot ({\x},{pow(\x, 2)});
    \end{tikzpicture}
  \end{center}

\section{Valeur absolue}

\begin{definition}
  La \emph{valeur absolue} d'un nombre réel $x$, noté, $\abs{x}$, est égale à
  \[
    \abs{x}=\left\{\begin{array}{cl}
      -x & \text{si $x<0$} \\
       x & \text{si $x\geq0$} \\
    \end{array}\right.
  \]
\end{definition}

\begin{propriete}
  Quels que soient $x$ et $y$ dans $\mathbb{R}$, on a :
  \begin{multicols}{2}
  \begin{itemize}[$\bullet$]
    \item $\abs{x}\geq0$ ;
    \item $\abs{x}=\abs{-x}$ ;
    \item $\abs{xy}=\abs{x}\abs{y}$ ;
    \item si $y\neq0$, $\abs{\frac{x}{y}}=\frac{\abs{x}}{\abs{y}}$.
  \end{itemize}
\end{multicols}
\end{propriete}

\begin{definition}
  La \emph{fonction valeur absolue} est la fonction définie sur $\mathbb{R}$ qui a tout $x$ associe sa valeur absolue $\abs{x}$.
\end{definition}

\begin{propriete} Les variations de la fonction valeur absolue sont :
  \begin{center}
    \begin{tikzpicture}
      \tkzTabInit[lgt=2,espcl=2]
      {$x$ /1,
        $\abs{x}$ /2}
      {$-\infty$,$0$,$+\infty$}%
      \tkzTabVar{+/, -/0, +/}
    \end{tikzpicture}
    \hfill
    \begin{tikzpicture}[domain=-2:2]
      \draw[dotted,color=gray] (-2.1,-1.1) grid (2.1,2.1);
      \draw[] (-2.2,0) -- (2.2,0);
      \draw[] (0,-1.2) -- (0,2.2);
      \draw[thick,->] (0,0) -- (1,0) node[midway,below]{$\vecteur\imath$};
      \draw[thick,->] (0,0) -- (0,1) node[midway,left]{$\vecteur\jmath$};
      \draw[domain=-2:2,smooth,blue] plot ({\x},{abs(\x)});
    \end{tikzpicture}
  \end{center}
\end{propriete}

\begin{propriete}[Équations]
  Quels que soient $x$ et $y$ dans $\mathbb{R}$, on a :
  \begin{multicols}{2}
  \begin{itemize}[$\bullet$]
    \item $\abs{x}=0$ ssi $x=0$ ;
    \item $\abs{x}=y$ ssi $\left\{\begin{array}{lcl}
        x=y & \text{ si $x\geq0$} \\
        -x=y & \text{ si $x<0$} \\
      \end{array}\right.$ ;
    \item $\abs{x}=\abs{y}$ ssi $x=y$ ou $x=-y$ ;
    \item $\abs{x+y}\neq\abs{x}+\abs{y}$ en général.
  \end{itemize}
\end{multicols}
\end{propriete}


\begin{propriete}[Équations]
  Soit $x\in\mathbb{R}$ tel que $\abs{x}=a$ (où $a\in\mathbb{R}$). Alors :
  \begin{itemize}[$\bullet$]
    \item si $a<0$, l'équation n'a pas de solutions ;
    \item si $a=0$, $x=0$ ;
    \item si $a>0$, $x=a$ ou $x=-a$.
  \end{itemize}
\end{propriete}


\end{document}
