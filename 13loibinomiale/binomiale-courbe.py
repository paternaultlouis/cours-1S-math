#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Génère quelques loi de probabilités de lois binomiales.
"""

import numpy
from scipy import special

for N, p in [
        (30, .4),
        (120, .4),
        (240, .4),
        (100, .3),
        (100, .5),
        (100, .8),
    ]:

    def binomiale(n):
        """Retourne la proba P(X=n), où X suit B(n,p)."""
        return special.binom(N, n)*p**n*(1-p)**(N-n)

    numpy.savetxt(
        "binomiale_{}_0,{}.dat".format(N, int(10*p)),
        zip(range(1, N), map(binomiale, range(1, N))),
        fmt='%0.5f',
        )
