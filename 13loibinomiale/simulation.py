#!/usr/bin/env python3

from random import random

p = 0.7
n = 500

succes = [0] * (n+1)
nb_simulations = 10000


for nb in range(nb_simulations):
    pile = 0
    for i in range(n):
        if random() < p:
            pile = pile + 1
    succes[pile] = succes[pile] + 1

for k in range(0, n):
    print(k, succes[k]/nb_simulations)
