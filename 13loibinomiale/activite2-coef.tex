%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2017 Louis Paternault --- http://ababsurdo.fr
%
% Publié sous licence Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)
% http://creativecommons.org/licenses/by-sa/4.0/deed.fr
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Pour compiler :
%$ lualatex $basename

% Beaucoup inspiré du travail de David Robert
% http://perpendiculaires.free.fr/wp-content/1S2014Chap15LoiBinomiale.pdf

\documentclass[12pt]{article}

\usepackage{1920-pablo}
\usepackage[a5paper,margin=1.4cm]{geometry}

\pagestyle{empty}
\usepackage{1920-pablo-paternault}
\begin{document}

\begin{center}
  \large\textsc{Coefficients binomiaux}
  \hrule
\end{center}

\section{Définition}

\begin{definition}[Coefficient binomial]
  Soient $k$ et $n$ deux entiers naturels, tels que $k\leq n$. On considère l'arbre représentant un schéma de Bernoulli de coefficients $n$ et $p$ (pour un certain $p$ réel de $[0, 1]$).

  On appelle \emph{coefficient binomial} de $k$ et $n$, noté $\binom{n}{k}$ (anciennement $C^k_n$), le nombre de chemins de cet arbre réalisant $k$ succès.
\end{definition}

\begin{propriete}[Dénombrement]
  Le nombre de combinaisons de $k$ éléments parmi $n$ est égal à $\binom{n}{k}$.

  Pour cette raison, $\binom{n}{k}$ est parfois prononcé \emph{« $k$ parmi $n$ »}.
\end{propriete}

\begin{exemple}~
  \begin{itemize}
    \item Le nombre de couples de couleurs différentes parmi un choix de six couleurs est \dotfill%$\binom{6}{2}=15$.
    \item Le tirage du loto est un tirage de sept numéros parmi quarante. Le nombre de tirages possibles est \dotfill %$\binom{40}{7}\approx19\times10^8$.
  \end{itemize}
\end{exemple}

\section{Propriétés}

\subsection{Travail préliminaire}

Réaliser, sur une feuille de brouillon, \emph{proprement}, les arbres correspondant à deux schéma de Bernoulli de paramètres respectifs $n=3$ et $p$ d'une part, et $n=4$ et $p$ d'autre part.

\subsection{Valeurs « remarquables »}

\emph{Pour chacune des questions suivantes, essayer d'abord avec des valeurs de $n$ particulières (par exemple $n=2$, $n=3$), puis en déduire une généralisation avec un $n$ quelconque.}

Soit un schéma de Bernoulli de coefficients $n$ et $p$ (pour un entier strictement positif $n$, et un réel $p$ de $\left[ 0;1 \right]$).

\begin{enumerate}
  \item Donner toutes les issues possibles à 0 succès. En déduire $\binom{n}{0}$.
  \item Donner toutes les issues possibles à 1 succès. En déduire $\binom{n}{1}$.
  \item Donner toutes les issues possibles à $n$ succès. En déduire $\binom{n}{n}$.
\end{enumerate}

\subsection{Coefficients « symétriques »}

\begin{enumerate}
  \item Soit un schéma de Bernoulli de paramètres $n=3$ et $p\in\left[ 0;1 \right]$.
    \begin{enumerate}
      \item Lister tous les évènements possibles à 1 succès, puis tous les évènements possibles à $3-1$ succès. Apparier les éléments des deux listes.
      \item En déduire une relation entre $\binom{3}{1}$ et $\binom{3}{2}$.
    \end{enumerate}
  \item Mêmes questions avec $n=4$, et les évènements à 3 succès d'une part, puis les évènements à $4-3$ succès d'autre part.
  \item En déduire une relation entre $\binom{n}{p}$ et $\binom{n}{n-p}$ (pour un entier $p$ de $\left[ 0;n \right]$).
\end{enumerate}

\subsection{Calcul pratique de coefficients binomiaux}

\begin{enumerate}
  \item On s'intéresse à l'arbre correspondant à un schéma de Bernoulli de paramètres $n=3$ et $p$ quelconque.
    \begin{enumerate}
      \item Masquer les branches les plus à droite de l'arbre. La partie visible correspond à un arbre d'un schéma de Bernoulli de paramètres $n=2$, pour le même $p$.
      \item Marquer en rouge les chemins à un succès. En déduire $\binom{2}{1}$.
      \item Marquer en vert les chemins à zéro succès. En déduire $\binom{2}{0}$.
      \item Révéler la partie droite de l'arbre, compter les branches à deux succès, et en déduire $\binom{3}{1}$.
      \item En déduire une relation entre $\binom{3}{1}$, $\binom{2}{1}$ et $\binom{2}{0}$.
    \end{enumerate}
  \item On s'intéresse à l'abre correspondant à un schéma de Bernoulli de paramètres $n=4$, et $p$ quelconque.
    \begin{enumerate}
      \item Masquer les branches les plus à droite de l'arbre.
      \item Marquer en rouge les chemins à trois succès. En déduire $\binom{3}{3}$.
      \item Marquer en rouge les chemins à deux succès. En déduire $\binom{3}{2}$.
      \item Révéler la partie droite de l'arbre, compter les branches à trois succès, et en déduire $\binom{4}{3}$.
      \item En déduire une relation entre $\binom{4}{3}$, $\binom{3}{3}$ et $\binom{3}{2}$.
    \end{enumerate}
  \item \emph{Généralisation :} Soient $n\in\mathbb{N}^*$ et $p$ un entier dans $\left[ 0;n \right]$. Exprimer $\binom{n+1}{p}$ en fonction de $\binom{n}{?}$ et $\binom{n}{?}$ (en remplaçant les $?$ par les bonnes valeurs).
\end{enumerate}

\section{Triangle de Pascal}

\vfill

\section{Bilan}

\begin{propriete}
  Soient $k$ et $n$ deux entiers naturels, tels que $k\leq n$. Alors

  \begin{enumerate}[(i)]
    \item $\binom{n}{0}=\binom{n}{n}=\ldots$
    \item $\binom{n}{1}=\ldots$
    \item $\binom{n}{n-k}=\ldots$
    \item $\binom{n}{k}+\binom{n}{k+1}=\ldots$
  \end{enumerate}
\end{propriete}

\end{document}
