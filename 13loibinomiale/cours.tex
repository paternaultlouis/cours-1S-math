%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2016 Louis Paternault --- http://ababsurdo.fr
%
% Publié sous licence Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)
% http://creativecommons.org/licenses/by-sa/4.0/deed.fr
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Pour compiler :
%$ lualatex $basename

\documentclass[12pt]{article}

\usepackage{1920-pablo}
\usepackage{1920-pablo-cours}
\usepackage{1920-pablo-paternault}
\usepackage[]{geometry}

\begin{document}

\section{Loi de Bernoulli}

\begin{definition}
  Une \emph{épreuve de Bernoulli} de paramètre $p$ (compris entre 0 et 1) est une expérience aléatoire comportant deux issues (le succès et l'échec), telle que la probabilité de succès est égale à $p$.
\end{definition}

\begin{exemple}~
  \begin{itemize}
    \item Le lancer d'une pièce de monnaie équilibrée, où l'obtention de « pile » est un succès, est une épreuve de Bernoulli de paramètre $\frac{1}{2}$.
    \item Essayer de deviner la valeur d'une carte tirée au hasard dans un jeu de 52 cartes est une expérience de Bernoulli de paramètre $\frac{1}{13}$.
  \end{itemize}
\end{exemple}

\begin{definition}
  À une épreuve de Bernoulli de paramètre $p$, on associe la variable aléatoire $X$ comptabilisant le nombre de succès. On appelle \emph{loi de Bernoulli} la loi suivie par cette variable aléatoire $X$.
\end{definition}

\begin{propriete}
  Soit $X$ une variable aléatoire suivant une loi de Bernoulli de paramètre $p$. Sa loi de probabilité est :
  \[
    \left\{
      \begin{array}{rcl}
        P(X=1) &=& p\\
        P(X=0) &=& 1-p\\
        P(X=x) &=& 0 \text{ si $x$ n'est ni 1, ni 0}
      \end{array}
      \right.
  \]
\end{propriete}

\begin{propriete}Soit $X$ une variable aléatoire suivant une loi de Bernoulli de paramètre $p$.
  \begin{itemize}[$\bullet$]
    \item $E(X)=p$ ;
    \item $V(X)=p(1-p)$ ;
    \item $\sigma(X)=\sqrt{p(1-p)}$.
  \end{itemize}
\end{propriete}

\section{Loi binomiale}

\begin{definition}
  Soit une suite d'expériences aléatoires. Si le résultat d'une expérience ne dépend pas des précédentes, ces expériences sont dites \emph{indépendantes}.
\end{definition}

\begin{definition}
  On appelle \emph{schéma de Bernoulli} de paramètres $n$ et $p$ la répétition de $n$ expériences de Bernoulli de paramètre $p$, indépendantes.
\end{definition}

\begin{exemple}
  Le lancer huit fois de suite d'une pièce de monnaie équilibrée, où l'obtention d'un pile représente un succès, est un schéma de Bernoulli de paramètres 8 et $\frac{1}{2}$.
\end{exemple}

\begin{definition}
  À un schéma de Bernoulli de paramètres $n$ et $p$, on associe la variable aléatoire $X$ comptabilisant le nombre de succès. On appelle \emph{loi binomiale} la loi suivie par cette variable aléatoire $X$.
\end{definition}

\begin{definition}[Coefficient binomial]
  Soient $k$ et $n$ deux entiers naturels, tels que $k\leq n$. On considère l'arbre représentant un schéma de Bernoulli de coefficients $n$ et $p$ (pour un certain $p$ réel de $[0, 1]$).

  On appelle \emph{coefficient binomial} de $k$ et $n$ le nombre de chemins de cet arbre réalisant $k$ succès.
\end{definition}

\begin{exemple}~

  \begin{multicols}{2}
    L'arbre ci-contre représente un schéma de Bernoulli de paramètres 3 et $p$ (par exemple, on lance une pièce de monnaies trois fois de suite). Les $S$ représentent les succès, les $E$ les échecs. Le nombre de droite correspond au nombre de succès de chaque branche.

    Cet arbre nous donne les valeurs suivantes des coefficients binomiaux.

    \begin{multicols}{2}
      \begin{itemize}[$\bullet$]
        \item $\binom{3}{0} = 1$
        \item $\binom{3}{1} = 3$
        \item $\binom{3}{2} = 3$
        \item $\binom{3}{3} = 1$
      \end{itemize}
    \end{multicols}

    \begin{center}
      \begin{tikzpicture}[grow=right, sloped]
        % Set the overall layout of the tree
        \tikzstyle{level 1}=[level distance=1.5cm, sibling distance=2.8cm]
        \tikzstyle{level 2}=[level distance=2cm, sibling distance=1.4cm]
        \tikzstyle{level 3}=[level distance=2.5cm, sibling distance=0.7cm]
        \node {}
        child {
          node {E}
          child {
            node {E}
            child {
              node {E --- 0}
              edge from parent
            }
            child {
              node {S --- 1}
              edge from parent
            }
            edge from parent
          }
          child {
            node {S}
            child {
              node {E --- 1}
              edge from parent
            }
            child {
              node {S --- 2}
              edge from parent
            }
            edge from parent
          }
          edge from parent
        }
        child {
          node {S}
          child {
            node {E}
            child {
              node {E --- 1}
              edge from parent
            }
            child {
              node {S --- 2}
              edge from parent
            }
            edge from parent
          }
          child {
            node {S}
            child {
              node {E --- 2}
              edge from parent
            }
            child {
              node {S --- 3}
              edge from parent
            }
            edge from parent
          }
          edge from parent
        };
      \end{tikzpicture}
    \end{center}
  \end{multicols}
\end{exemple}


\begin{propriete}
  Soit $X$ une variable aléatoire suivant une loi binomiale de paramètres $n$ et $p$. Sa loi de probabilité est :
  \[
    \left\{
      \begin{array}{rcl}
        P(X=k) &=& \binom{n}{k}p^k(1-p)^{n-k} \text{ si $k$ est un entier compris entre 0 et $n$}\\
        P(X=x) &=& 0 \text{ sinon}
      \end{array}
    \right.
  \]
\end{propriete}
\begin{demo}
  On considère l'arbre correspondant au schéma de Bernoulli de paramètres $n$ et $p$ (où $n$ est un entier, et $p$ un réel).

  Soit $k$ un entier compris entre 0 et $n$. Par définition du coefficient binomial, $\binom{n}{k}$ branches comportent $k$ succès. De plus, chacune de ces branches comporte $k$ succès et $n-k$ échecs, donc sa probabilité est $p^k(1-p)^{n-k}$. En sommant les probabilités de ces branches, on obtient : $P(X=k)=\binom{n}{k}p^k(1-p)^{n-k}$.
\end{demo}

\begin{propriete}
  Soit $X$ une variable aléatoire suivant une loi binomiale de paramètres $n$ et $p$.
  \begin{itemize}
    \item $E(X)=np$
    \item $V(X)=np(1-p)$
    \item $\sigma(X)=\sqrt{np(1-p)}$
  \end{itemize}
\end{propriete}

\begin{propriete}
  Représentation graphique loi binomiale TODO
\end{propriete}

\section{Coefficient binomial}

\begin{propriete}
  Le nombre de combinaisons de $k$ éléments parmi $n$ est égal à $\binom{n}{k}$.

  Pour cette raison, $\binom{n}{k}$ est parfois prononcé \emph{« $k$ parmi $n$ »}.
\end{propriete}

\begin{exemple}~
  \begin{itemize}
    \item Sachant qu'une main de tarot (à cinq joueurs) est constituées de quinze cartes d'un jeu de soixante-dix-huit, le nombre de mains possible est égal à $\binom{78}{5}\approx4,4\times10^{15}$.
    \item Le tirage du loto est un tirage de sept numéros parmi quarante. Le nombre de tirages possibles est $\binom{40}{7}\approx19\times10^8$.
  \end{itemize}
\end{exemple}

\begin{propriete}
  Soient $k$ et $n$ deux entiers naturels, tels que $k\leq n$. Alors

  \begin{enumerate}[(i)]
    \item $\binom{n}{k}=\frac{n!}{k!(n-k)!}$
    \item $\binom{n}{0}=\binom{n}{n}=1$
    \item $\binom{n}{1}=n$
    \item $\binom{n}{k}=\binom{n}{n-k}$
    \item $\binom{n}{k}+\binom{n}{k+1}=\binom{n+1}{k+1}$
  \end{enumerate}
\end{propriete}

\begin{corollaire}Pour calculer à la main les coefficients binomiaux, on peut tracer le triangle de Pascal :

  \begin{multicols}{2}
    \begin{center}
      \begin{tikzpicture}[x=13mm,y=9mm]
        % some styles
        \tikzset{
          box/.style={
            minimum height=5mm,
            inner sep=.7mm,
            outer sep=0mm,
            text width=10mm,
            text centered,
            line width=.25mm,
          },
          link/.style={-latex,line width=.3mm},
        }
        % Pascal's triangle
        % row #0 => value is 1
        \node[box] (p-0-0) at (0,0) {1};
        \foreach \row in {1,...,5} {
          % col #0 =&gt; value is 1
          \node[box] (p-\row-0) at (-\row/2,-\row) {1};
          \pgfmathsetmacro{\pascalvalue}{1};
          \foreach \col in {1,...,\row} {
            % iterative formula : val = precval * (row-col+1)/col
            % (+ 0.5 to bypass rounding errors)
            \pgfmathtruncatemacro{\pascalvalue}{\pascalvalue*((\row-\col+1)/\col)+0.5};
            \global\let\pascalvalue=\pascalvalue
            % position of each value
            \coordinate (pos) at (-\row/2+\col,-\row);
            % odd color for odd value and even color for even value
            \pgfmathtruncatemacro{\rest}{mod(\pascalvalue,2)}
            \ifnum \rest=0
            \node[box] (p-\row-\col) at (pos) {\pascalvalue};
            \else
            \node[box] (p-\row-\col) at (pos) {\pascalvalue};
            \fi
          }
        }
      \end{tikzpicture}
    \end{center}

    \begin{itemize}
      \item les nombres aux extrémités de chaque ligne sont des 1 ;
      \item chaque autre nombre est la somme des deux nombres au dessus-de lui.
    \end{itemize}

    \noindent\emph{Comment lire cette figure :} La valeur du coefficient
    binomial $\binom{n}{k}$ est la $(k+1)$\up{e} valeur de la $(n+1)$\up{e}
    ligne.

  \end{multicols}

\end{corollaire}

\section{Échantillonage}

\begin{activite}
  Activité informatique sur tableur.
\end{activite}


\begin{definition}
  Soient :
  \begin{itemize}[$\bullet$]
    \item $X$ une variable aléatoire suivant une loi binomiale de paramètres $p$ et $n$ ;
    \item $a$ le plus petit entier tel que $P(X\leq a)>0,025$ ;
    \item $b$ le plus petit entier tel que $P(X\leq b)\geq0,975$.
  \end{itemize}
  L'intervalle $\left[\frac{a}{n};\frac{b}{n}\right]$ est appelé \emph{intervalle de fluctuation à 95~\% de $\frac{X}{n}$}.
\end{definition}

\begin{remarque}
  Cela signifie que la probabilité que la variable aléatoire $\frac{X}{n}$ appartienne à $\left[\frac{a}{n};\frac{b}{n}\right]$ est supérieure à 95~\%. En d'autres termes :
  $P\left(a\leq X\leq b\right)\geq0,95$.
\end{remarque}

\begin{methode}[Règle de décision]
  Soit une population dont on suppose qu'une proportion $p$ des individus présente un certain caractère. On prélève un échantillon de $n$ individus, et on note $f$ la fréquence d'apparation de ce caractère dans cet échantillon.

  On considère $X$ une variable aléatoire suivant une loi binomiale de paramètres $n$ et $p$, et on note $a$ et $b$ les entiers tels que définis dans la définition précédente.

  Si $f$ n'appartient pas à l'intervalle de fluctuation $\left[\frac{a}{n};\frac{b}{n}\right]$, l'hypothèse sur la valeur de la proportion $p$ de la population rejetée au seuil de 5~\% (il y a une probabilité de 5~\% de faire une erreur en rejetant cette hypothèse). Sinon, elle est acceptable.
\end{methode}


\end{document}
