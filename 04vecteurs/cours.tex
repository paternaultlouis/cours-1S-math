%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2016 Louis Paternault --- http://ababsurdo.fr
%
% Publié sous licence Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)
% http://creativecommons.org/licenses/by-sa/4.0/deed.fr
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Pour compiler :
%$ lualatex $basename

\documentclass[12pt]{article}

\usepackage{1920-pablo}
\usepackage{1920-pablo-cours}
\usepackage{1920-pablo-paternault}
\usepackage[a4paper, margin=3cm]{geometry}

\begin{document}

\begin{remarque}
  Le plan est muni d'un repère $(O, I, J)$ quelconque (pas nécessairement normé, pas nécessairement orthogonal).
\end{remarque}

\section{Colinéarité}

\begin{definition}
  Deux vecteurs $\vecteur{u}$ et $\vecteur{v}$ sont \emph{colinéaires} si et seulement si il existe $k$ réel tel que $\vecteur{u}=k\vecteur{v}$ ou $\vecteur{v}=k\vecteur{u}$.
\end{definition}

\begin{propriete}~
  \begin{enumerate}[(i)]
    \item Le vecteur nul $\vecteur{0}$ est colinéaire avec tout vecteur $\vecteur{u}$.
    \item Si deux vecteurs (non nuls) sont colinéaires, tout vecteur (non nul) colinéaire à l'un est colinéaire à l'autre.
    \item \hp{} On dit que la colinéarité est une \emph{relation transitive} parmi les vecteurs non nuls.
  \end{enumerate}
\end{propriete}
\begin{demo}(partielle…)
  \begin{enumerate}[(i)]
    \item Pour n'importe quel vecteur $\vecteur{u}$, on a : $\vecteur{0}=0\times\vecteur{u}$.
  \end{enumerate}
\end{demo}

\begin{propriete}[Condition de colinéarité]
  Deux vecteurs $\vecteur{u}\coord{x}{y}$ et $\vecteur{v}\coord{x'}{y'}$ sont colinéaires si et seulement si $xy'-x'y=0$.
\end{propriete}
\begin{demo}

  \paragraph{Premier cas : l'un des deux vecteurs est nul}. Alors ses coordonnées sont $\coord{0}{0}$, les deux vecteurs sont colinéaires, et nécessairement $xy'-x'y=0$.

  \paragraph{Second cas : les deux vecteurs sont non nuls}.
  Soient $\vecteur{u}\coord{x}{y}$ et $\vecteur{v}\coord{x'}{y'}$, non nuls, colinéaires, et $k$ le réel tel que $\vecteur{u}=k\vecteur{v}$. Alors $\systeme{x=kx'}{y=ky'}$, et $xy'-x'y=kx'y'-kx'y'=0$.

  \emph{La réciproque est laissée au lecteur patient…}
\end{demo}

\section{Équations de droites}

\begin{activite}[TODO À voir]
  Soit une droite définie par deux points $A(x_A, y_A)$ et $B(x_B, y_B)$. À quelle condition un point $M(x,y)$ appartient-il à $\mathcal{D}$ ?

  Le point $M$ appartient à $\mathcal{D}$ si et seulement si $A$, $B$ et $M$ sont alignés, c'est-à-dire si et seulement si les vecteurs $\vecteur{AM}$ et $\vecteur{AB}$ sont colinéaires.

  TODO

  \ldots si et seulement si $ax+by+c=0$.
\end{activite}

\begin{defprop}
  Toute droite $\mathcal{D}$ du plan admet une équation \emph{cartésienne} de la forme $ax+by+c=0$, où l'un (au moins) des nombres $a$ et $b$ est non nul. La droite est constituée de l'ensemble des points du plan de coordonnées $\left( x, y \right)$ vérifiant l'équation.
\end{defprop}

\begin{defprop}~
  \begin{itemize}[$\bullet$]
    \item On appelle \emph{vecteur directeur} d'une droite $\mathcal{D}$ du plan tout vecteur $\vecteur{AB}$, où $A$ et $B$ sont deux points distincts de $\mathcal{D}$.
    \item Une droite d'équation cartésienne $ax+by+c=0$ admet un vecteur directeur de coordonnées $\left( -b; a \right)$.
  \end{itemize}
\end{defprop}

\begin{propriete}[Caractérisation d'une droite]
  Une droite du plan peut-être caractérisée par :
  \begin{itemize}[$\bullet$]
    \item deux points distincts ;
    \item un point et un vecteur directeur (non nul) ;
    \item une équation cartésienne ;
    \item une équation réduite.
  \end{itemize}
  Seule la caractérisation par équation réduite est unique.
\end{propriete}

\section{Décomposition de vecteurs}

\begin{propriete}
  Soient $\vecteur{u}$ et $\vecteur{v}$ deux vecteurs non colinéaires. Alors tout vecteur $\vecteur{w}$ peut être décomposé de façon unique comme $\vecteur{w}=a\vecteur{u}+b\vecteur{v}$, où $a$ et $b$ sont réels.
\end{propriete}

\begin{definition}~
  \begin{enumerate}[(i)]
    \item On appelle \emph{repère du plan} la donnée d'un point $O$ et de deux vecteurs non colinéaires $\vecteur{u}$ et $\vecteur{v}$.
    \item Pour tout point $M$ du plan, $\vecteur{OM}=a\vecteur{u}+b\vecteur{v}$ s'écrit aussi : « $M$ a pour coordonnées $(a, b)$ dans le repère $(O, \vecteur{u}, \vecteur{v})$ ».
  \end{enumerate}
\end{definition}

\begin{propriete}~
  \begin{enumerate}[(i)]
    \item Une droite de vecteur directeur $\vecteur{u}\left( x; y \right)$ (avec $x\neq0$) a pour coefficient directeur $\frac{y}{x}$.
    \item Une droite de coefficient directeur $m$ a un vecteur directeur de coordonnées $\left( 1; m \right)$.
  \end{enumerate}
\end{propriete}

\begin{propriete}
Soient $d_1$ et $d_2$ deux droites. Les propositions suivantes sont équivalentes.
\begin{enumerate}[(i)]
  \item Les deux droites sont parallèles.
  \item Les deux droites ont deux vecteurs directeurs colinéaires.
  \item Les deux droites ont tous leurs vecteurs directeurs colinéaires.
  \item S'ils existent, les coefficents directeurs des deux droites sont égaux.
\end{enumerate}
\end{propriete}

\end{document}
