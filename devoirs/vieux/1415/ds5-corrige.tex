%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2015 Louis Paternault --- http://ababsurdo.fr
%
% Publié sous licence Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)
% http://creativecommons.org/licenses/by-sa/4.0/deed.fr
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Pour compiler :
%$ lualatex $basename

\documentclass[12pt]{article}

\usepackage{1920-pablo}
\usepackage{1920-pablo-devoir}
\usepackage{1920-pablo-paternault}
\usepackage[a5paper,margin=1.9cm]{geometry}

\pagestyle{empty}

\title{Produit scalaire Statistiques}
\date{15/01/15}
\classe{1\up{e}S3}
\dsnum{DS \no5}

\begin{document}

\maketitle

\begin{exercice}[Dérivées --- 4 points]
  \emph{Calculer la dérivée des fonctions suivantes.}
  \begin{enumerate}
    \item $f:x\mapsto 3x^2-2\sqrt{x}$
      \begin{align*}
        f'(x) &= 3\times2x-2\times\frac{1}{2\sqrt{x}} \\
              &= 6x-\frac{1}{\sqrt{x}}
      \end{align*}
    \item $g:x\mapsto \left( 2x-1 \right)\left( 1-x \right)$
      \begin{align*}
        g'(x) &= 2\left( 1-x \right)+\left( -1 \right)\left( 2x-1 \right) \\
              &= 2-2x-2x+1\\
              &= -4x+3
      \end{align*}
      Il était aussi possible de commencer par développer $g$, puis de dériver la forme développée, qui est un polynôme. Le résultat doit être le même.
  \end{enumerate}
\end{exercice}

\begin{exercice}[Temps d'attente --- 4 points]
  \begin{em}
  Une gérante veut mettre en place les files d'attente dans son nouveau magasin. Elle hésite entre deux méthodes :
  \begin{enumerate}
    \item il y a une file d'attente pour chaque caisse (comme dans les supermarchés) ;
    \item il y a une seule file d'attente pour toutes les caisses, et les clients sont répartis au dernier moment vers une caisse libre (comme dans les gares \textsc{sncf}).
  \end{enumerate}

  Pour choisir, elle simule une fois pour chacune des deux méthodes, l'arrivée de 1000 clients, dans les mêmes conditions. Elle observe les temps d'attente suivants pour la file type « supermarché » :

  \vspace{1em}
  \hspace{-2cm}\begin{tabular}{l|cccccccccc}
    Temps d'attente & 1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 & 9 & 10 \\
    \hline
    Effectif & 32 & 101 & 150 & 145 & 98 & 122 & 123 & 101 & 60 & 68
  \end{tabular}
  \vspace{1em}

  Pour la file de type « \textsc{sncf} », elle obtient un temps d'attente moyen de 5,4 minutes, et un écart-type de 1,9 minutes.
\end{em}

  \begin{enumerate}
    \item \emph{Calculer la moyenne et l'écart-type des temps d'attente de la file type « supermarché ».} À la calculatrice, on trouve $\bar{x}=5,375$ et $\sigma\approx2,49$.
    \item \emph{Quelle type de file choisiriez-vous ? Justifier.} La moyenne est sensiblement la même (ou alors est exactement la même si 5,4 minutes est en fait un arrondi de 5,375). À temps d'attente moyen égal, il est préférable d'avoir un écart-type petit. Un grand écart type signifie qu'il y a de grandes disparités : certaines files vont avancer très vite, et d'autres très lentement. Cela va engendrer un sentiment de frustration. Avec une file unique, en revanche (et un écart-type petit), tout le monde attend (à peu de choses près) aussi longtemps.
  \end{enumerate}

\end{exercice}

\begin{exercice}[Parallélogramme --- 6 points]
  \emph{On considère le parallélogramme suivant.}

  \begin{center}
    \begin{tikzpicture}[very thick, scale=.8]
      \coordinate (A) at (0,0);
      \coordinate (B) at (4,0);
      \coordinate (D) at (1,2);
      \coordinate (C) at ($(D)+(B)-(A)$);

      \draw (A) node[below left]{$A$}
      -- (B) node[below, midway]{4} node[below right]{$B$}
      -- (C) node[above right]{$C$}
      -- (D) node[above left]{$D$}
      -- cycle node[midway, left]{3};
      \draw (A) -- (C) node[midway, below]{6};
    \end{tikzpicture}
  \end{center}

  \begin{enumerate}
    \item \emph{Montrer que $\vecteur{AB}.\vecteur{AD}=5,5$.}
      \begin{align*}
        \vecteur{AB}.\vecteur{AD} &= \frac{1}{2}\left( \norme{\vecteur{AB}+\vecteur{AD}}^2-AB^2-AD^2 \right)\\
                                  &= \frac{1}{2}\left( \norme{\vecteur{AC}}^2-AB^2-AD^2 \right)\\
                                  &= \frac{1}{2}\left( 6^2-4^2-3^2 \right)\\
                                  &= 5,5
      \end{align*}
    \item \emph{En utilisant une autre expression du produit scalaire $\vecteur{AB}.\vecteur{AD}$, en déduire la mesure de l'angle $\widehat{BAD}$ (arrondi à 0,1\degre{} ou 0,01 radians près).}
      Nous utilisons la formule du cosinus :
      \begin{align*}
        \vecteur{AB}.\vecteur{AD} &= AB.AD.\cos\widehat{BAD} \\
                                  &= 4\times3\times\cos\widehat{BAD}\\
                                  &= 12 \cos\widehat{BAD}
      \end{align*}
      En utilisant le résultat précédent, cela donne $5,5=12\cos\widehat{BAD}$, soit $\cos\widehat{BAD}=\frac{5,5}{12}$. Une mesure de l'angle $\widehat{BAD}$ est donc 62,7\degre{} ou 1,09 rad.
    \item 
      \begin{enumerate}
        \item \emph{Développer $\left( \vecteur{BA}+\vecteur{AD} \right)^2$.} C'est une identité remarquable : $\left( \vecteur{BA}+\vecteur{AD} \right)^2=BA^2+2\vecteur{BA}.\vecteur{AD}+AD^2$.
        \item \emph{En déduire la valeur exacte de la longueur $BD$.} Nous remarquons que $\vecteur{BA}+\vecteur{AD}=\vecteur{BD}$, et que $\vecteur{AB}.\vecteur{AD}=-\vecteur{BA}.\vecteur{AD}$.
          \begin{align*}
            \left( \vecteur{BA}+\vecteur{AD} \right)^2 &= BA^2+2\vecteur{BA}.\vecteur{AD}+AD^2 \\
            BD^2 &= BA^2-2\vecteur{AB}.\vecteur{AD}+AD^2\\
            BD^2 &= 4^2-2\times5,5+3^2\\
            BD^2 &= 14\\
            BD &= \sqrt{14}
          \end{align*}
      \end{enumerate}
  \end{enumerate}

\end{exercice}

\begin{exercice}[Lieu géométrique --- 6 points]~

  \begin{multicols}{2}
    \emph{Dans la figure ci-contre, on place un point $M$ sur la droite d'équation $y=x$ ; ses coordonnées sont donc $(x,x)$ (où $x$ est un réel). Le but de l'exercice est de déterminer les positions possibles de $M$ telles que les droites $(AM)$ et $(BM)$ soient perpendiculaires.}

    \columnbreak

  \begin{center}
    \begin{tikzpicture}[scale=0.8, thick]
      \draw[dotted] (-0.5,-0.5) grid (3.5,3.5);
      \draw[->] (-1,0) -- (4,0);
      \draw[->] (0,-1) -- (0,4);
      \draw (0,0) node[below left]{$O$};
      \draw (1,0) node[below]{$1$};
      \draw (0,1) node[left]{$1$};

      \draw (2,3) node{$\bullet$} node[above left]{$A$};
      \draw (3,1) node{$\bullet$} node[below right]{$B$};
      \draw[dashed] (-1,-1) -- (4,4);
    \end{tikzpicture}
  \end{center}
  \end{multicols}

      \begin{enumerate}
        \item \emph{Montrer que $\vecteur{AM}.\vecteur{BM}=\left( x-3 \right)\left( 2x-3 \right)$.} Les coordonnées de $A$, $B$ et $M$ sont respectivement $A\left( 2;3 \right)$, $B\left( 3;1 \right)$ et $M\left( x;x \right)$. Donc les coordonnées des vecteurs $\vecteur{AM}$ et $\vecteur{BM}$ sont $\vecteur{AB}\left( x-2;x-3 \right)$ et $\vecteur{BM}\left( x-3;x-1 \right)$. Donc :
          \begin{align*}
            \vecteur{AM}.\vecteur{BM} &= \left( x-2 \right)\left( x-3 \right)+\left( x-3 \right)\left( x-1 \right) \\
                                      &= \left( x-3 \right)\left( \left( x-2 \right)+\left( x-1 \right) \right)\\
                                      &= \left( x-3 \right)\left( 2x-3 \right)
          \end{align*}
        \item \emph{Résoudre    $\vecteur{AM}.\vecteur{BM}=0$.} Puisque $\vecteur{AM}.\vecteur{BM}=\left( x-3 \right)\left( 2x-3 \right)$, c'est une équation produit, dont les solutions sont $x=3$ et $x=\frac{3}{2}$.
        \item \emph{Répondre au problème posé : Quelles sont les positions possibles de $M$ ?} Les droites $\left( AM \right)$ et $\left( BM \right)$ sont perpendiculaires si et seulement si les vecteurs correspondants sont orthogonaux, c'est-à-dire si et seulement si leur produit scalaire est nul. Donc $x=3$ ou $x=\frac{3}{2}$, et donc les coordonnées possible de $M$ sont $\left( 3;3 \right)$ et $\left( \frac{3}{2};\frac{3}{2} \right)$.
      \end{enumerate}

\end{exercice}


\end{document}
