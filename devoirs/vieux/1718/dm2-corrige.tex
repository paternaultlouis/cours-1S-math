%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2017 Louis Paternault --- http://ababsurdo.fr
%
% Publié sous licence Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)
% http://creativecommons.org/licenses/by-sa/4.0/deed.fr
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Pour compiler :
%$ lualatex $basename
%$ lualatex $basename

\documentclass[12pt]{article}

\usepackage{1920-pablo}
\usepackage{1920-pablo-devoir}
\usepackage{1920-pablo-paternault}
\usepackage{1920-pablo-listings}
\usepackage[a5paper,margin=1.6cm]{geometry}

\title{\large Suites arithmétiques\\Corrigé}
\date{}
\classe{1\up{e}S}
\dsnum{DM \no2}

\pagestyle{empty}
\setlength{\parindent}{0pt}

\let\oldmethode\Methode
\renewcommand{\Methode}[1]{\oldmethode{\emph{(#1)}}}

\begin{document}

\maketitle

\begin{exercice}
\begin{em}
Sam joue au jeu vidéo \emph{Cat Lady}, dans lequel elle doit aider une vieille dame en ramenant ses chats qui se sont enfuis.

Elle doit ramener 2 chats dans le premier niveau du jeu ; 5 dans le second niveau ; 8 dans le troisième ; et ainsi de suite.

On appelle $u$ la suite définie sur $\mathbb{N}^*$ par : $u_n$ est le nombre de chats que Sam doit capturer au niveau $n$.
\end{em}

\begin{enumerate}
  \item \emph{Donner les valeurs de $u_1$ et $u_5$.} Le terme $u_1$ est le nombre de chats à ramener au premier niveau, soit $u_1=5$. Puisqu'il y a trois chats de plus à ramener à chaque niveau, on a $u_2=5$, $u_3=8$, $u_4=11$, $u_5=14$.
  \item \emph{On admet que $u$ est une suite arithmétique. Préciser ses paramètres (premier terme et raison).} Il faut ramener à chaque niveau trois chats de plus qu'au précédent (par exemple : $u_1-u_0=5-2=3$), donc la raison est 3. Le premier terme est $u_1=2$.
  \item \emph{Exprimer $u_n$ en fonction de $n$.} C'est une suite arithmétique, donc $u_n=u_0+(n-1)r=2+3(n-1)=2+3n-3=3n-1$.
  \item \emph{Sam a fini les cinquante niveaux du jeu. Combien de chats a-t-elle ramené au total ?} On doit calculer la somme des cinquante premiers termes de la suite :
\begin{align*}
  \sum\limits_{n=1}^{50}u_n&=50\times\frac{u_1+u_{50}}{2}\\
&= 50\times \frac{2+3\times50-1}{2}\\
&= 3775
\end{align*}
Elle a donc ramené au total 3775 chats.
\end{enumerate}
\end{exercice}

\begin{exercice}[Problème ouvert]~
  \begin{multicols}{2}
\begin{em}
    Une petite fille empile ses cubes comme indiqué sur la figure ci-contre (chaque étage contient deux cubes de plus que l'étage du dessus).
\end{em}

  \columnbreak

  \begin{center}
    \begin{tikzpicture}[scale=.6, ultra thick]
      \foreach \i/\n in {1/1, 2/3, 3/5, 4/7} {
        \foreach \j in {1, ..., \n} {
          \draw ({\j-\n/2}, -\i) rectangle ++(1, 1);
        }
      }
    \end{tikzpicture}
  \end{center}
  \end{multicols}

\begin{em}
    Combien d'étage aura la plus grande pyramide qu'elle pourra construire avec 1729 cubes ? Combien de cubes seront alors inutilisés ?
\end{em}

\Methode{Avec les suites}\label{methode:suites} On appelle $u$ la suite définie sur $\mathbb{N}^*$ par : \emph{$u_n$ est le nombre de cubes de l'étage $n$ (en partant du haut)}. C'est une suite arithmétique de premier terme\footnote{Remarque : On aurait tout aussi bien pu prendre comme premier terme $u_0$ (et non pas $u_1$), mais cela aurait induit un décalage entre lu numéro de l'étage et l'indice du terme de la suite (le nombre de termes du 7\up{e} étage aurait alors été $u_6$ et non pas $u_7$, par exemple).} $u_1=1$ et de raison 2. Donc pour n'importe quel nombre $n$, on a $u_n=1+2(n-1)=1+2n-2=2n-1$.

Une pyramide de $n$ étage contiendra donc $u_1+u_2+\cdots+u_n$ cubes. Puisque la petite fille dispose de 1729 cubes, alors on a :
\begin{align*}
u_1+u_2+\cdots+u_n &\leq 1729 \\
n\times\frac{u_1+u_n}{2} &\leq 1729 \\
n\times\frac{1+2n-1}{2} &\leq 1729 \\
n\times\frac{2n}{2} &\leq 1729\\
n^2&\leq 1729\\
n&\leq \sqrt{1729} \text{ car $n$ est un nombre positif.}
\end{align*}
Donc, puisque $\sqrt{1729}\approx41,6$, la plus grande valeur que peut prendre $n$ est 41.

La pyramide aura donc 41 étages.

Le nombre de cubes utilisés sera alors $41\times\frac{u_1+u_{41}}{2}=41\times\frac{1+2\times41-1}{2}=1681$, et il restera $1729-1681=48$ cubes inutilisés.

\Methode{Avec le tableur} On construit la feuille de calcul suivante. La première colonne est le numéro de l'étage, la colonne A est le nombre de cubes de cet étage, et la colonne B le nombre de cube de la pyramide qui a ce nombre d'étage.

Pour remplir ce tableur, les cellules $A1$ et $B1$ contiennent les nombres 1, et on écrit dans la cellule $A2$ : \texttt{=A1+2}, et dans la cellule $B2$ : \texttt{=B1+A2}. On fait ensuite glisser les formules pour les recopier vers le bas.

\begin{multicols}{2}
Par exemple, la ligne 3 signifie : « Le 3\up{e} étage est constitué de 5 cubes, et il faut 9 cubes pour fabriquer une pyramide à 3 étages ».

On observe que la ligne 42 est la première à dépasser le nombre 1729. La plus grande pyramide possible a donc 41 étages.

\columnbreak
\begin{center}
\begin{tabular}[h]{|>{\columncolor[gray]{.9}}c|*{2}{>{\hfill}p{1cm}|}}
  \hline
  \rowcolor[gray]{.9}&A&B\\ \hline
  1&1&1\\ \hline
  2&3&4\\ \hline
  3&5&9\\ \hline
  \multicolumn{3}{c}{$\cdots$}\\ \hline
  41&81&1681\\ \hline
  42&83&1764\\ \hline
  43&85&1849\\ \hline
\end{tabular}
\end{center}
\end{multicols}

\Methode{Avec la géométrie}\emph{Cette méthode manque de rigueur, mais la démarche est intéressante.}

  \begin{center}
    \begin{tikzpicture}[scale=.6, ultra thick]
      \foreach \i/\n in {1/1, 2/3, 3/5, 4/7} {
        \foreach \j in {1, ..., \n} {
          \draw ({\j-\n/2}, -\i) rectangle ++(1, 1);
        }
      }
    \draw[dashed] (0, 0.5) -- (5, -4.5);
    \end{tikzpicture}
    \hfill
    \begin{tikzpicture}[scale=.6, ultra thick]
      \foreach \i/\n in {2/2, 3/4, 4/6} {
        \foreach \j in {1, ..., \n} {
          \draw ({\j-\n/2-.5}, -\i) rectangle ++(1, 1);
        }
	\draw[fill=gray] ({-.5-\n/2}, -\i) -- ++(1, 0) -- ++(0, 1) -- cycle;
	\draw ({.5+\n/2}, -\i) -- ++(0, 1) -- ++(1, -1) -- cycle;
      }
      \draw[fill=gray] (-.5, -1) -- ++(1, 1) -- ++(0, -1) -- cycle;
      \draw (0.5, 0) -- ++(1, -1);
    \end{tikzpicture}
  \end{center}

Sur chacun des étages, on découpe le demi-cube de droite (figure de gauche), puis on le « recolle » à la gauche du même étage (figure de droite). On obtient donc un triangle, dont l'aire est le nombre de cubes de la pyramide.

La base de ce triangle est le nombre de cubes du dernier étages, plus un, soit (en utilisant la suite de la méthode \ref{methode:suites}), 2n-1+1=2n.

La hauteur de ce triangle est le nombre d'étages, soit $n$.

Donc l'aire du triangle, qui correspond au nombre de cubes, est donc $\frac{2n\times n}{2}=n^2$.

Puisque la petite fille dispose de 1729 cubes, on cherche le plus grand $n$ tel que $n^2\leq 1729$, c'est-à-dire (puisque $n$ est positif), le plus grand $n$ tel que $n\leq\sqrt{1729}$. Puisque $\sqrt{1729}\approx 41,6$, alors $n=41$.

La plus grande pyramide possible a 41 étages.

%\Methode{Avec les carrés}\emph{Le raisonnement ici est le même que celui de la méthode \ref{methode:suites}, mais avec peu de rigueur.}
%On calcule, à la main, le nombre de cubes nécessaire pour faire une pyramide de quelques étages.
%
%On obtient :
%pour 1 étage, 1 cube ;
%pour 2 étage, 4 cubes ;
%pour 3 étage, 9 cubes ;
%pour 4 étage, 16 cubes ; etc.
%On remarque\footnote{Pour être rigoureux, il faudrait prouver cette relation. C'est l'objet de la méthode \ref{methode:suites}.} que pour $n$ étages, il faut $n^2$ cubes.
%
%Puisque l'on dispose de 1729 cubes, on cherche le plus grand $n$ tel que $n^2\leq1729$, c'est-à-dire (puisque $n$ est positif), le plus grand $n$ tel que $n\leq\sqrt{1729}$. Enfin, puisque $\sqrt{1729}\approx41,6$, cela donne $n=41$.
%
%La plus grande pyramide possible possède 41 étages.

\Methode{Avec un algorithme} On met en œuvre l'algorithme suivant, sur calculatrice ou sur ordinateur, où la variable \emph{etage} représente le numéro de l'étage, \emph{ligne} le nombre de cube à cet étage, et \emph{total} le nombre total de cubes de la pyramide jusqu'à présent.

  \begin{lstlisting}[frame=single,mathescape=true, language=naturel]
  etage <- 1
  ligne <- 1
  total <- 1
  TANTQUE total $\leq$ 1729
      etage <- etage + 1
      ligne <- ligne + 2
      total <- total + ligne
  FinTantque
  Afficher(etage)
  Afficher(ligne)
  Afficher(total)
  \end{lstlisting}

Ce programme « ajoute » des lignes à la pyramide jusqu'à ce qu'elle dépasse 1729 ; il donne comme résultat : $etage=42$, $ligne=83$, $total=1764$, ce qui signifie que le premier étage qui utilise plus de 1729 cubes est le 42\up{e}, composé de 83 cubes. Il faut donc le retirer, et cela signifie que la pyramide aura $42-1=41$ étages composés de $1764-83=1681$ cubes. Il restera donc $1729-1681=48$ cubes inutilisés.
\end{exercice}

\end{document}
