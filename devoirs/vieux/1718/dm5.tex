%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2017 Louis Paternault --- http://ababsurdo.fr
%
% Publié sous licence Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)
% http://creativecommons.org/licenses/by-sa/4.0/deed.fr
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Pour compiler :
%$ lualatex $basename
%$ lualatex $basename

\documentclass[11pt]{article}

\usepackage{1920-pablo}
\usepackage{1920-pablo-devoir}
\usepackage{1920-pablo-paternault}
\usepackage[a5paper,margin=1cm]{geometry}
\setlength{\parindent}{0pt}

\title{Dérivation}
\date{11/01/18}
\classe{1\up{e}S}
\dsnum{DM \no5}

\pagestyle{empty}
\begin{document}

\maketitle

\begin{exercice}[Application à la physique]\label{ex:newton}~

  \begin{multicols}{2}
    \begin{em}
      Remarque : Si cette méthode fonctionne \emph{en théorie} pour déterminer la valeur de $g$, elle est très peu précise, et il a toujours existé d'autres manières plus précises pour déterminer $g$.
    \end{em}

  Isaac voudrait déterminer la valeur de $g$, intensité de la pesanteur, chez
  lui. Pour cela, il lâche une pomme du haut du puits d'une mine à Pendleton
  (Grande-Bretagne), haut de \numprint[m]{1024}, et chronomètre son temps de
  chute.

  L'altitude de la pomme est mesurée à partir du fond du puits : elle est de
  \numprint[m]{0} au fond, et \numprint[m]{1024} en haut.

    \begin{center}
    \begin{tikzpicture}[baseline=(O.base)]
      \coordinate (O) at (0,5);
      % Puits
      \draw[thick] (0.3,5) -- (0.5,5) -- (0.5,0) -- (1.5,0) -- (1.5,5) -- (1.7,5);

      % Axe
      \draw[thick,->] (0,0) -> (0,6);
      \draw (0,0) node {$-$} node[left]{0\hspace{0.3em}~};
      \draw (0,5) node {$-$} node[left]{1024\hspace{0.3em}~};
      \draw (0,5.5) node[above left] {\emph{z (m)}\hspace{0.3em}~};

      % Pomme
      \draw[fill,gray] (1,5) circle (0.15);
      \draw[gray] (1.05,5.25) arc (110:180:0.1);

      % Vitesse
      \draw[thick, ->] (1,4.7) -> (1,4);
    \end{tikzpicture}
  \end{center}

\end{multicols}

  Isaac sait que cette altitude en fonction du temps est un polynôme de la
  forme $z:t\mapsto at^2+bt+c$, où $t$ est le temps de chute.  Par exemple,
  $z(0)$ est l'altitude initiale, et $z(3)$ est l'altitude après trois
  secondes de chute. Le but de l'exercice est de déterminer les valeurs de
  $a$, $b$ et $c$, pour en déduire la valeur de l'intensité de la pesanteur
  $g$.


  \begin{enumerate}[(1)]
    \item Combien vaut l'altitude initiale $z(0)$ ? En déduire que $c=1024$.
    \item La vitesse $v$ de la chute est égale à la dérivée de la fonction
      $z$. Par exemple, $v(2)=z'(2)$ est la vitesse de la pomme après deux
      secondes de chute.
      \begin{enumerate}[(a)]
        \item Dériver $z$, et en déduire l'expression de $v$ en fonction de
          $a$ et $b$.
        \item Quelle est la vitesse initiale ? En déduire que $b=0$.
        \item Exprimer $z$ et $v$ en fonction de $a$ et $t$.
      \end{enumerate}
    \item Isaac, aidé de Gottfried, a mesuré que la chute a duré  \numprint[s]{14,5}. Traduire
      cette information par une équation, et montrer que $a=-4,87$.
    \item Calculer la dérivée de $v$ ; c'est une constante égale à $-g$.
      Conclure en déterminant la valeur de $g$. \emph{Bonus (optionnel) : Quelle est l'unité de $g$ ?}
  \end{enumerate}
\end{exercice}

\begin{exercice}[Calcul de fonction dérivées]\label{ex:derivee}~
\begin{em}
  Répondre à deux des trois questions suivantes (1, 2 et 3), classées par ordre de difficulté croissante. La méthode à appliquer est celle utilisée dans le cours, pour démontrer que la dérivée de la fonction carrée est la fonction $x\mapsto 2x$.

Ne faites la question (c) que pour une des deux questions, au choix.
\end{em}
  \begin{enumerate}
    \item On considère la fonction $f:x\mapsto x^2-3x+1$, définie sur $\mathbb{R}$, et $a$ un réel.
      \begin{enumerate}
        \item Montrer que pour un réel $h$ non nul, le taux d'accroissement en $a$ est égal à $2a+h-3$.
        \item\label{q:limite} En déduire la valeur de $\lim\limits_{h\rightarrow0} \frac{f(a+h)-f(a)}{h}$ (en fonction de $a$), et donc la valeur de $f'\left( a \right)$.
        \item\label{q:application} \emph{Application :} Calculer $f'(2)$, et tracer dans un repère orthonormé la courbe de $f$ (sur l'intervalle $\left[ 0;4 \right]$), ainsi que sa tangente en 2.
      \end{enumerate}
    \item On considère la fonction $f:x\mapsto \frac{1}{x}$, définie sur $\mathbb{R}^*$, et $a$ un réel non nul.
      \begin{enumerate}
        \item Montrer que pour un réel $h$ non nul (et tel que $a+h\neq0$), le taux d'accroissement en $a$ est égal à $-\frac{1}{a\left( a+h \right)}$.
        \item Même énoncé que la question \ref{q:limite}.
        \item Même énoncé que la question \ref{q:application}.
      \end{enumerate}
    \item On considère la fonction $f:x\mapsto \sqrt{x}$, définie sur $\mathbb{R}^+$, et $a$ un réel strictement positif.
      \begin{enumerate}
        \item Montrer que pour un réel $h$ non nul (et tel que $a+h\geq0$), le taux d'accroissement en $a$ est égal à $\frac{1}{\sqrt{a}+\sqrt{a+h}}$.
        \item Même énoncé que la question \ref{q:limite}.
        \item Même énoncé que la question \ref{q:application}.
      \end{enumerate}
  \end{enumerate}
\end{exercice}


\begin{exercice}[Exercices libres]
  Choisir un exercice sur le site web \url{http://pyromaths.org}, imprimer l'énoncé, résoudre cet exercice, et \emph{corriger vous-même} votre travail (je ne le corrigerai pas moi-même, sauf, si vous le mentionnez dans votre copie). Rendre l'énoncé avec la copie.

  Par exemple (mais vous pouvez aussi faire un \emph{autre} exercice que celui-ci) :
  \begin{itemize}
    \item \emph{Classe de première S} $\rightarrow$ \emph{Nombre dérivé graphiquement} : pour travailler le chapitre en cours.
  \end{itemize}
\end{exercice}
\end{document}
