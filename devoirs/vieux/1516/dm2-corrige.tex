%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2015 Louis Paternault --- http://ababsurdo.fr
%
% Publié sous licence Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)
% http://creativecommons.org/licenses/by-sa/4.0/deed.fr
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Pour compiler :
%$ lualatex $basename
%$ lualatex $basename

\documentclass[12pt]{article}

\usepackage{1920-pablo}
\usepackage{1920-pablo-devoir}
\usepackage{1920-pablo-paternault}
\usepackage[a5paper,margin=0.9cm]{geometry}

\title{Généralités sur les fonctions}
\date{05/11/15}
\classe{1\up{e}S6}
\dsnum{DM \no2}

\pagestyle{empty}
\begin{document}

\maketitle

\begin{exercice}[Fonction cube]
  \begin{em}
  On appelle \emph{fonction cube} la fonction définie sur $\mathbb R$ par
  $x\mapsto x^3$.
\end{em}
  \begin{enumerate}[(a)]
    \item \emph{Conjecturer, à l'aide de la calculatrice, les variations de cette fonction sur $\mathbb R$.} La fonction semble strictement croissante sur $\mathbb{R}$ (voir la courbe tracée à la question (\ref{question:calculatrice})).
    \item \label{question:opposes}\emph{Premier cas : $a$ et $b$ sont de signes opposés. Justifier que, si $a<0<b$, alors $a^3<b^3$.} Puisque $a<0<b$, et que $a^2$ et $b^2$ sont strictement positifs (car ce sont des carrés), alors $a^3=a\times a^2$ est strictement négatif, et $b^3=b\times b^2$ est strictement positif. Donc $a^3<b^3$.
    \item \emph{Second cas  : $a$ et $b$ sont de même signe.}
      \begin{enumerate}[(i)]
        \item \emph{Montrer que pour tous réels $a$ et $b$, on a : \[a^3-b^3=(a-b)(a^2+ab+b^2)\]}
          Développons le membre de droite :
          \begin{align*}
              (a-b)(a^2+ab+b^2) &= a^3+a^2b+ab^2-ba^2-ab^2-b^3\\
              &= a^3-b^3
          \end{align*}
        \item \emph{Quel est le signe de $a^2+ab+b^2$ si $a$ et $b$ sont de même signe ?} Dans ce cas, alors le produit $ab$ est strictement positif (si $a$ et $b$ sont négatifs, c'est le produit de deux nombres négatifs ; s'ils sont positifs, c'est le produit de deux nombres positifs). Donc la somme $a^2+ab+b^2$ est strictement positive (en tant que somme de termes strictement positifs).
      \end{enumerate}
    \item \emph{Déduire des questions précédentes que, pour tous réels $a$ et $b$ tels que $a<b$, on a $a^3<b^3$. Conclure.}

      Soient $a$ et $b$ deux réels positifs tels que $a<b$. Deux cas sont possibles.
      \begin{description}
        \item[Premier cas : $a$ et $b$ sont de signes différents] Nous avons montré à la question (\ref{question:opposes}) que $a^3<b^3$.
        \item[Deuxième cas : $a$ et $b$ sont de même signe]
          Puisque $a<b$, alors $a-b<0$. Or nous avons montré que $a^2+ab+b^2>0$, donc $\left( a-b \right)\left( a^2+ab+b^2 \right)<0$. Donc $a^3-b^3<0$, et $a^3<b^3$.
      \end{description}
      Dans les deux cas, si $a<b$, alors $a^3<b^3$ : la fonction cube est strictement croissante sur $\mathbb{R}$.
    \item \emph{Établir le tableau de variation de la fonction cube.}
      \begin{center}
    \begin{tikzpicture}
      \tkzTabInit[lgt=2,espcl=2]
      {$x$ /1,
      $x\mapsto x^3$ /1
      }
      {$-\infty$,$0$,$+\infty$}%
      \tkzTabVar{-/, R/, +/}
      \tkzTabVal{1}{3}{0.5}{}{0}
    \end{tikzpicture}
  \end{center}
\item \label{question:calculatrice}
    \emph{Tracer sur l'écran de la calculatrice les courbes représentatives des fonctions carré et cube. Étudier (par le calcul) les positions relatives de ces deux courbes.}

  \begin{multicols}{2}
      La courbe de la fonction carrée est strictement au dessus de celle de la fonction cube si et seulement si $x^2> x^3$.
      \begin{align*}
        x^2 &> x^3\\
        x^2-x^3 &> 0\\
        x^2(1-x) &> 0
      \end{align*}
      Si $x$ est nul ou égal à 1, alors $x^2=x^3$, et les deux courbes sont confondues. Sinon, $x^2$ est strictement positif, et donc $x^2(1-x)>0$ si et seulement si $1-x>0$, c'est-à-dire si $x<1$. Donc les courbes des deux fonctions sont confondues en 0 et 1, et la courbe de la fonction carrée est au dessus (strictement) de celle de la fonction cube si et seulement si $x<1$.

      \columnbreak

  \begin{center}
      \begin{tikzpicture}[domain=-2:2, thick]
        \draw[dotted,color=gray] (-2.1,-4.1) grid (2.1,4.1);
        \draw[] (-2.2,0) -- (2.2,0);
        \draw[] (0,-4.2) -- (0,4.2);
        \draw[thick,->] (0,0) -- (1,0) node[midway,below]{$\vecteur\imath$};
        \draw[thick,->] (0,0) -- (0,1) node[midway,left]{$\vecteur\jmath$};
        \draw[domain={-4^(1/3)}:{4^(1/3)},smooth,blue] plot ({\x},{\x*\x*\x});
        \draw[domain=-2:2,smooth,blue] plot ({\x},{\x*\x});
      \end{tikzpicture}
    \end{center}
  \end{multicols}
  \end{enumerate}
\end{exercice}

\pagebreak

\begin{exercice}[Fraction rationnelle et Identification]
  \begin{em}
  Le but de l'exercice est de déterminer les variations de la fonction définie sur $\mathbb{R}\backslash\left\{ 2 \right\}$ par : \[f:x\mapsto\frac{2x-7}{x-2}\]

  Pour résoudre ce problème, nous allons exprimer la fonction $f$ sous la forme $a+\frac{b}{x-2}$, où $a$ et $b$ sont des nombres réels.
\end{em}

  \begin{enumerate}[(a)]
    \item \emph{Réduire l'expression $a+\frac{b}{x-2}$ au même démonimateur.}
      \begin{align*}
        a+\frac{b}{x-2} &= \frac{a\left( x-2 \right)}{x-2} +\frac{b}{x-2} \\
        &= \frac{ax-2a}{x-2} + \frac{b}{x-2} \\
        &= \frac{ax-2a+b}{x-2}
      \end{align*}
    \item \emph{En déduire les valeurs de $a$ et $b$ pour que $a+\frac{b}{x-2}=f\left( x \right)$.}
      On a : $a+\frac{b}{x-2}=f\left( x \right)=\frac{2x-7}{x-2}$. Or nous venons de démontrer que $a+\frac{b}{x-2}=\frac{ax-2a+b}{x-2}$. Donc :
      \[\frac{2x-7}{x-2}=\frac{ax-2a+b}{x-2}\]
      Pour que ceci soit vrai, \emph{par identification}, on a : $2=a$ (car $2x=ax$) et $-7=-2a+b$, donc $b=-3$.

      Donc $f\left( x \right)=2-\frac{3}{x-2}$.
    \item \emph{En utilisant les variations des fonctions associées, en déduire les variations de la fonction $f$.}

      \begin{center}
    \begin{tikzpicture}
      \tkzTabInit[lgt=2,espcl=2]
      {$x$ /1,
      $x-2$ /1,
      $\frac{1}{x-2}$ /1,
      $-\frac{3}{x-2}$ /1,
      $2-\frac{3}{x-2}$ /1
      }
      {$-\infty$,$2$,$+\infty$}%
      \tkzTabVar{-/, R/, +/}
      \tkzTabVal{1}{3}{0.5}{}{0}
      \tkzTabVar{+/, -D+/, -/}
      \tkzTabVar{-/, +D-/, +/}
      \tkzTabVar{-/, +D-/, +/}
    \end{tikzpicture}
  \end{center}

  La fonction $f$ est donc croissante sur $\left] -\infty; 2 \right[$ et sur $\left] 2; +\infty \right[$.
  \end{enumerate}
\end{exercice}

\end{document}
