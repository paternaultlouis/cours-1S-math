%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2016 Louis Paternault --- http://ababsurdo.fr
%
% Publié sous licence Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)
% http://creativecommons.org/licenses/by-sa/4.0/deed.fr
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Pour compiler :
%$ lualatex $basename
%$ lualatex $basename

\documentclass[12pt]{article}

\usepackage{1920-pablo}
\usepackage{1920-pablo-devoir}
\usepackage{1920-pablo-paternault}
\usepackage[a5paper, margin=1.4cm]{geometry}
\usepackage{unicode-math}

\title{Dérivation}
\date{07/01/16}
\classe{1\up{e}S6}
\dsnum{DM \no4}

\pagestyle{empty}
\begin{document}

\maketitle

\begin{exercice}[Calcul de fonction dérivées]\label{ex:derivee}~

  \begin{enumerate}
    \item \emph{On considère la fonction $f:x\mapsto x^2$, définie sur $\mathbb{R}$, et $a$ un réel.}
      \begin{enumerate}
        \item \emph{Montrer que pour un réel $h$ non nul, le taux d'accroissement en $a$ est égal à $2a+h$.}
        Calculons le taux d'accroissement :
          \begin{align*}
            \frac{f(a+h)-f(a)}{h} &=\frac{\left( a+h \right)^2 - a^2}{h}\\
            &= \frac{a^2+2ah+h^2-a^2}{h}\\
            &= \frac{2ah+h^2}{h}\\
            &= 2a+h
          \end{align*}
        \item\label{q:limite} \emph{En déduire la valeur de $\lim\limits_{h\rightarrow0} \frac{f(a+h)-f(a)}{h}$ (en fonction de $a$), et donc la valeur de $f'\left( a \right)$.}
        Lorsque $h$ tend vers 0, $2a+h$ tend vers $2a$. Donc $\lim\limits_{h\rightarrow0} \frac{f(a+h)-f(a)}{h}=2a$. Nous avons montré que $f'(a)=2a$.
        \item\label{q:application} \emph{\emph{Application :} Calculer $f'(2)$, et tracer dans un repère orthonormé la courbe de $f$ (sur l'intervalle $\left[ 0;4 \right]$), ainsi que sa tangente en 2.} Le nombre dérivé de $f$ en 2 est $f'(2)=2\times2=4$. Voir le graphique à la fin.
      \end{enumerate}
    \item \emph{On considère la fonction $f:x\mapsto \frac{1}{x}$, définie sur $\mathbb{R}^*$, et $a$ un réel non nul.}
      \begin{enumerate}
        \item \emph{Montrer que pour un réel $h$ non nul (et tel que $a+h\neq0$), le taux d'accroissement en $a$ est égal à $-\frac{1}{a\left( a+h \right)}$.}
          \begin{align*}
            \frac{f(a+h)-f(a)}{h} &= \frac{\frac{1}{a+h}-\frac{1}{a}}{h}\\
            &= \frac{\frac{a}{a\left( a+h \right)}-\frac{a+h}{a\left( a+h \right)}}{h}\\
            &= \frac{\frac{a-a-h}{a\left( a+h \right)}}{h}\\
            &= \frac{-h}{a\left( a+h \right)}\times\frac{1}{h}\\
            &= -\frac{1}{a\left( a+h \right)}\\
          \end{align*}
        \item \emph{Même énoncé que la question \ref{q:limite}.}
        Lorsque $h$ tend vers 0, $a+h$ tend vers $a$, donc $-\frac{1}{a\left( a+h \right)}$ tend vers $-\frac{1}{a^2}$. Donc $f'(a)=-\frac{1}{a^2}$.
        \item \emph{Même énoncé que la question \ref{q:application}.}
        Le nombre dérivé de $f$ en 2 est : $f'(2)=-\frac{1}{2^2}=-\frac{1}{4}$. Voir le graphique à la fin.
      \end{enumerate}
    \item \emph{On considère la fonction $f:x\mapsto \sqrt{x}$, définie sur $\mathbb{R}^+$, et $a$ un réel strictement positif.}
      \begin{enumerate}
        \item \emph{Montrer que pour un réel $h$ non nul (et tel que $a+h\geq0$), le taux d'accroissement en $a$ est égal à $\frac{1}{\sqrt{a}+\sqrt{a+h}}$.}
        Calculons le taux d'accroissement :
        \begin{align*}
            \frac{f\left( a+h \right)-f\left( a \right)}{h}
            &= \frac{\sqrt{a+h}-\sqrt{a}}{h} \\
            &= \frac{\sqrt{a+h}-\sqrt{a}}{h}\times\frac{\sqrt{a+h}+\sqrt{a}}{\sqrt{a+h}+\sqrt{a}} \\
            &= \frac{\sqrt{a+h}^2-\sqrt{a}^2}{h\left( \sqrt{a+h}+\sqrt{a} \right)}\\
            &= \frac{h}{h\left( \sqrt{a+h}+\sqrt{a} \right)}\\
            &= \frac{1}{        \sqrt{a+h}+\sqrt{a} }
        \end{align*}
        \item \emph{Même énoncé que la question \ref{q:limite}.}
        Lorsque $h$ tend vers 0, $\sqrt{a+h}$ tend vers $\sqrt{a}$, donc le taux d'accroissement tend vers $\frac{1}{2\sqrt{a}}$. Donc $f'(a)=\frac{1}{2\sqrt{a}}$.
        \item \emph{Même énoncé que la question \ref{q:application}.}
        Le nombre dérivé de $f$ en 2 vaut : $f'(2)=\frac{1}{2\sqrt{2}}\approx 0,35$.
          Voir le graphique ci-dessous.
      \end{enumerate}
  \end{enumerate}
  \begin{center}
    \begin{tikzpicture}[ultra thick, xscale=1]
      \begin{axis}%
        [
          ultra thick,
          grid=both,
          axis equal image,
          minor x tick num=1, % 4 minor ticks => 5 subintervals
          xmin=0,
          xmax=5,
          xscale=2,
          minor y tick num=1,  % 4 minor ticks => 5 subintervals
          ymin=-.5,
          ymax=7.5,
          axis lines=middle,
          no markers,
          samples=100,
          domain=0:5,
        ]
        \addplot[blue](x,{x*x});
        \addplot[blue,dashed](x,{4*(x-2)+4});
        \draw (axis cs: 2,4)  node{$\bullet$};
        \draw (axis cs: 2,4) node[right]{Fonction carrée};

        \addplot[green](x,{1/x});
        \addplot[green,dashed](x,{-.25*(x-2)+1/2});
        \draw (axis cs: 2,{1/2})  node{$\bullet$};
        \draw (axis cs: 2,{1/2}) node[above right]{Fonction inverse};

        \addplot[red,](x,{sqrt(x)});
        \addplot[red,dashed](x,{(x-2)/(2*sqrt(2))+sqrt(2)});
        \draw (axis cs: 2,{sqrt(2)})  node{$\bullet$};
        \draw (axis cs: 5,{sqrt(5)}) node[above left]{Fonction racine carrée};
      \end{axis}
    \end{tikzpicture}
  \end{center}
\end{exercice}

\pagebreak

\begin{exercice}[Application à la physique]\label{ex:newton}~

  \begin{em}
  \begin{multicols}{2}
  Isaac voudrait déterminer la valeur de $g$, intensité de la pesanteur, chez
  lui. Pour cela, il lâche une pomme du haut du puits d'une mine à Pendleton
  (Grande-Bretagne), haut de \numprint[m]{1024}, et chronomètre son temps de
  chute.

  L'altitude de la pomme est mesurée à partir du fond du puits : elle est de
  \numprint[m]{0} au fond, et \numprint[m]{1024} en haut.

    \begin{center}
    \begin{tikzpicture}[baseline=(O.base)]
      \coordinate (O) at (0,5);
      % Puits
      \draw[thick] (0.3,5) -- (0.5,5) -- (0.5,0) -- (1.5,0) -- (1.5,5) -- (1.7,5);

      % Axe
      \draw[thick,->] (0,0) -> (0,6);
      \draw (0,0) node {$-$} node[left]{0\hspace{0.3em}~};
      \draw (0,5) node {$-$} node[left]{1024\hspace{0.3em}~};
      \draw (0,5.5) node[above left] {\emph{z (m)}\hspace{0.3em}~};

      % Pomme
      \draw[fill,gray] (1,5) circle (0.15);
      \draw[gray] (1.05,5.25) arc (110:180:0.1);

      % Vitesse
      \draw[thick, ->] (1,4.7) -> (1,4);
    \end{tikzpicture}
  \end{center}

\end{multicols}

  Isaac sait que cette altitude en fonction du temps est un polynôme de la
  forme $z:t\mapsto at^2+bt+c$, où $t$ est le temps de chute.  Par exemple,
  $z(0)$ est l'altitude initiale, et $z(3)$ est l'altitude après trois
  secondes de chute. Le but de l'exercice est de déterminer les valeurs de
  $a$, $b$ et $c$, pour en déduire la valeur de l'intensité de la pesanteur
  $g$.
\end{em}


  \begin{enumerate}[(1)]
    \item \emph{Combien vaut l'altitude initiale $z(0)$ ? En déduire que $c=1024$.}
      Au départ, la pomme est au sommet du puit, donc $z\left( 0 \right)=1024$. En utilisant l'expression de $z$, on trouve que $z\left( 0 \right)=a\times0^2+b\times0+c=c$. Donc $c=1024$.
    \item \emph{La vitesse $v$ de la chute est égale à la dérivée de la fonction
      $z$. Par exemple, $v(2)=z'(2)$ est la vitesse de la pomme après deux
    secondes de chute.}
      \begin{enumerate}[(a)]
        \item \emph{Dériver $z$, et en déduire l'expression de $v$ en fonction de $a$ et $b$.}
          Puisque $z$ est un polynôme, on a : $z'\left( t \right)=2at+b$. Donc $v\left( t \right)=z'\left( t \right)=2at+b$.
        \item \emph{Quelle est la vitesse initiale ? En déduire que $b=0$.}
          La pomme est lâchée du sommet, donc $v(0)=0$. Or $v\left( 0 \right)=2a\times0+b=b$. Donc $b=0$.
        \item \emph{Exprimer $z$ et $v$ en fonction de $a$ et $t$.}
          Donc :
          \begin{align*}
v\left( t \right) &= 2at \\
z\left( t \right) &= at^2+1024
          \end{align*}
      \end{enumerate}
    \item \emph{Isaac, aidé de Gottfried, a mesuré que la chute a duré  \numprint[s]{14,5}. Traduire cette information par une équation, et montrer que $a=-4,87$.}
      La chute s'arrête lorsque la pomme atteint le fond du puits, c'est-à-dire quand $z\left( t \right)=0$. Donc, $z(14,5)=0$, et $a\times14,5^2+1024=0$. Donc $a=-\frac{1024}{14,5^2}\approx-4,87$.
    \item \emph{Calculer la dérivée de $v$ ; c'est une constante égale à $-g$.  Conclure en déterminant la valeur de $g$.}
      La dérivée de $v$ est $v'\left( t \right)=2a\approx2\times\left( -4,87 \right)\approx-9,74$. Donc $-g\approx-9,74$, et $g=9,74$.
  \end{enumerate}
\end{exercice}

\pagebreak

\begin{exercice}[Tangente et Jeu vidéo]\label{ex:avion}
  \begin{em}
La figure ci-dessous représente un écran de jeu vidéo. Un avion remonte l'écran de gauche à droite en suivant la courbe d'équation $y=-1-\dfrac{1}{x}$

L'avion peut tirer des missiles selon la tangente à sa trajectoire.

\begin{center}
  \begin{tikzpicture}[very thick, scale=.8]
    \newcommand\monstre{
      \begin{scope}[red, scale=.2, shift={(0,.4)}]
        \draw (-1.5, 0) -- (1.5, 0);
        \draw (1, 0) arc (0:180:1);
      \end{scope}
    }
    \draw[rounded corners] (0,0) rectangle (5, -5);
    \foreach \i in {1, 2, ..., 5} {
      \draw (\i, .5) node{\i};
      \draw (-.5, -\i) node{-\i};
    }
    \draw (-.5,.5) node{0};
    \begin{scope}[shift={(1,0)}]
      \monstre
    \end{scope}
    \begin{scope}[shift={(3,0)}]
      \monstre
    \end{scope}
    \draw[domain=.25:5,smooth,variable=\x,blue,opacity=.5] plot ({\x},{-1-1/\x});
    \draw[gray,dashed] (.5, {-1-1/.5}) -- ++({90-20}:3.2);
    \begin{scope}[shift={(.5, {-1-1/.5})},thick,scale=.07,rotate=-20]
    % Avion
      \draw[fill=lightgray!30!white] (0,0) -- ++(1,0) -- ++(0,1) -- ++(4,-1) -- ++(-5,10) -- ++(-5, -10) -- ++(4, 1) -- ++(0, -1) -- cycle;
    \end{scope}
    %\draw ({sqrt(2)-1}, {-2-sqrt(2)}) node{$\bullet$};
    %\draw (1, -2) node{$\bullet$};
  \end{tikzpicture}
\end{center}

En quels points de sa trajectoire l'avion doit-il tirer ses missiles pour abattre successivement les deux monstres situés en haut de l'écran en $A \coord{1}{0} , B \coord{3}{0}$ ?
\end{em}

Pour une abscisse $a$ donnée, l'avion tire un missile selon la tangente à sa trajectoire. Donc la trajectoire du missile a pour équation $y=f'(a)\left( x-a \right)+f(a)$ (où $f$ est la fonction représentant la trajectoire de l'avion). Il faut donc calculer la dérivée de $f$. Puisque la dérivée d'une constante est nulle, on a : $f'\left( x \right) = -\left( -\frac{1}{x^2} \right)=\frac{1}{x^2}$.

L'équation de la tangente est donc :
\begin{align*}
y&=f'(a)(x-a)+f(a) \\
\Leftrightarrow y&=\frac{1}{a^2}\left( x-a \right)-1-\frac{1}{a}\\
\Leftrightarrow y&=\frac{x-a}{a^2}-\frac{a^2}{a^2}-\frac{a}{a^2}\\
\Leftrightarrow y&=\frac{x-a-a^2-a}{a^2}\\
\Leftrightarrow y&=\frac{-a^2-2a+x}{a^2}\\
\end{align*}

Supposons que le missile, lancé depuis l'avion au point d'abscisse $a$, atteigne le monstre en $A\coord{1}{0}$. Cela signifie que ce monstre est sur sa trajectoire, c'est-à-dire que le point appartient à la tangente à la trajectoire de l'avion. C'est-à-dire :
\[0=\frac{-a^2-2a+1}{a^2}\]
Puisque $a$ est strictement positif (la trajectoire de l'avion n'est définie que pour $a>0$), l'équation est équivalente à 
\[0=-a^2-2a+1\]
C'est un trinôme du second degré. Son discriminant est $\Delta=\left( -2 \right)^2-4\times\left( -1 \right)\times1=8$. Il est positif, donc l'équation a deux solutions 
$a_1=\frac{-\left( -2 \right)-\sqrt{8}}{2\times-1}=\frac{2-2\sqrt{2}}{-2}=\sqrt{2}-1$ et
$a_2=\frac{-\left( -2 \right)+\sqrt{8}}{2\times-1}=\frac{2+2\sqrt{2}}{-2}=-\sqrt{2}-1$. Or cette dernière valeur est négative, donc exclue. Il n'y a donc qu'une seule solution $a_1=\sqrt{2}-1$.

Pour toucher le premier monstre, l'avion doit donc avoir comme abscisse $\sqrt{2}-1$, et comme ordonnée $-1-\frac{1}{\sqrt{2}-1}$ (que l'on peut simplifier en $-2-\sqrt{2}$).


De même, en appliquant le même raisonnement au second monstre, on trouve que l'avion doit être en $\coord{1}{-2}$ pour l'atteindre.
\end{exercice}

\pagebreak

\begin{exercice}[Droites et Jeu vidéo]\label{ex:forage}
  \begin{em}
  Chaïma joue au jeu vidéo \emph{Well Well Well Drilling}. Son personnage, situé sur le flanc d'une montagne, doit forer des puits, en ligne droite, afin d'exploiter différentes ressources.

  \begin{center}
    \begin{tikzpicture}[ultra thick, scale=0.19, x=1pt, y=1pt]

    \draw [gray, fill=black!10!white, rounded corners=7] (0,250) -- (50,300) -- (800, 738) -- (950, 580) -- (1024, 600) -- (1024, 0) -- (0, 0) -- cycle;
    \draw[rounded corners=7] (0, 0) rectangle (1024, 768);
    \draw (90, 312) node{$\bullet$} node[above]{$A$};
    \draw (770, 400) node{$\bullet$} node[left]{$C$};
    \draw (900, 120) node{$\bullet$} node[left]{$D$};
    %\draw (654, 650) node{$\bullet$} node[left]{$F$};
  \end{tikzpicture}
\end{center}

Sur son écran représenté ci-dessus, le flanc de la montagne est principalement constitué d'une pente rectiligne passant par $A\left( 90; 312 \right)$ et de pente 60~\%. Un filon de coltan est situé en $C\left( 770; 400 \right)$ et un autre de diamant en $D\left( 900, 120 \right)$ (le repère considéré a pour origine le coin inférieur gauche de l'écran, les axes sont les bords inférieur et gauche de l'écran, et l'unité est le pixel).

  Pour obtenir le trophée « Foreuse économe », Chaïma doit, en un seul forage, traverser à la fois le filon de coltan et celui de diamant. Quelles sont alors les coordonnées du point à partir duquel son personnage doit forer, et quel angle doit former le puits avec l'horizontale ?
\end{em}

Appelons $F$ le point où Chaïma doit forer. Ce point doit être à la fois sur le plan de la montagne, et aligné avec $C$ et $D$ (c'est-à-dire sur la droite $\left( CD \right)$).

\paragraph{Équation du flanc de la montagne.}
Commençons par déteminer l'équation du flanc de la montagne : c'est une droite, donc de la forme $y=mx+p$. Nous savons que la pente est 60~\%, donc $y=0,6x+p$. Enfin, nous savons que le point $A\left( 90;312 \right)$ appartient à cette droite, donc $312=0,6\times90+p$. En résolvant cette équation, nous trouvons $p=258$. L'équation est donc \fbox{$y=0,6x+258$}.

\paragraph{Équation de $\left( CD \right)$.}
Déterminons l'équation de $\left( CD \right)$. Son coefficient directeur vaut $\frac{y_D-y_C}{x_D-y_C}=\frac{120-400}{900-770}=-\frac{28}{13}$. L'équation de la droite est donc de la forme $y=-\frac{28}{13}x+p$. De plus, puisque $D$ est sur cette droite, ses coordonnées vérifient l'équation, donc $120=900\times-\frac{28}{13}+p$, ce qui nous donne $p=\frac{26760}{13}$. L'équation de la droite est donc \fbox{$y=-\frac{28}{13}x+\frac{26760}{13}$}.

\paragraph{Recherche des coordonnées de $F$.} Puisque $F$ est sur le flanc de la montagne, et sur la droite $\left( CD \right)$, ses coordonnées vérifient les deux équations.
\[\left\{\begin{array}{rl}
  y &= 0,6x+258\\
  y &= -\frac{28}{13}x+\frac{26760}{13}
\end{array}\right.\]

Une fois ce système résolu, nous obtenons environ : $F\left(654;650  \right) $. Reste à déterminer l'angle auquel forer.

\paragraph{Calcul de l'angle.} En zoomant sur la partie qui nous intéresse, nous obtenons ceci.

  \begin{center}
    \begin{tikzpicture}[ultra thick, scale=0.3, x=1pt, y=1pt]
      \clip (500, 300) rectangle (900, 700);
      \draw [gray, fill=black!10!white, rounded corners=7] (0,250) -- (50,300) -- (800, 738) -- (950, 580) -- (1024, 600) -- (1024, 0) -- (0, 0) -- cycle;
      \draw[rounded corners=7] (0, 0) rectangle (1024, 768);
      \draw (90, 312) node{$\bullet$} node[above]{$A$};
      \draw (770, 400) node{$\bullet$} node[left]{$C$};
      \draw (900, 120) node{$\bullet$} node[left]{$D$};
      \draw (654, 650) node{$\bullet$} node[left]{$F$};
      \draw (770, 650) node{$\bullet$} node[right]{$H$};
      \draw[dashed] (770, 650) -- (770, 400) -- (654, 650) -- cycle;
    \end{tikzpicture}
  \end{center}

  Ajoutons un point $H$ à la verticule de $C$ et à l'horizontale de $F$. Ses coordonnées sont donc $H\left( 770; 650 \right)$ (la même abscisse que $C$, et la même ordonnée que $F$). Puisque l'angle qui nous intéresse est l'angle de $\left( FC \right)$ par rapport à l'horizontale, c'est la mesure de l'angle $\widehat{HFC}$ que nous voulons calculer. Un peu de trigonométrie dans le triangle $FHC$, rectangle en $H$, va nous donner la solution.

  \begin{align*}
    \tan \widehat{HFC} &= \frac{HC}{FH} \\
    \widehat{HFC} &= \arctan\frac{HC}{FH} \\
    \widehat{HFC} &= \arctan\frac{650-400}{770-654} \\
    \widehat{HFC} &\approx 65°
  \end{align*}
  
  \paragraph{Conclusion.} Il faut forer à partir du point $F\left( 654, 650 \right)$, avec un angle d'environ 65° (vers le bas) avec l'horizontale.

\end{exercice}

\end{document}
