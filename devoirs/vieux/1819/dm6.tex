%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2019 Louis Paternault --- http://ababsurdo.fr
%
% Publié sous licence Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)
% http://creativecommons.org/licenses/by-sa/4.0/deed.fr
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Pour compiler :
%$ lualatex $basename
%$ lualatex $basename

\documentclass[10pt]{article}

\usepackage{1920-pablo}
\usepackage{1920-pablo-devoir}
\usepackage{1920-pablo-paternault}
\usepackage[a5paper,margin=1.0cm]{geometry}

\title{Probabilités\\Trigonométrie}
\date{13/03/19}
\classe{1\up{e}S}
\dsnum{DM \no6}

\pagestyle{empty}
\begin{document}

\maketitle

\begin{em}
  \noindent Faire l'un des exercices 1 ou 2 au choix (l'exercice 1 est plus difficile). Les exercices 3 et 4 sont obligatoires.
\end{em}

\begin{exercice}[Jeu infini]
  Une kermesse propose le jeu suivant : un joueur lance un dé équilibré à six faces. Si 2, 3, 4, 5 ou 6 est obtenu, il gagne le nombre de bonbons indiqués sur le dé et le jeu s'arrête. Si 1 est obtenu, il gagne un bonbon, et relance le dé pour le même jeu.

  Par exemple : une joueuse lance le dé et obtient 1 : elle gagne un bonbon et rejoue. Elle fait à nouveau 1 : elle gagne un second bonbon. Elle relance le dé et obtient 3 : elle gagne trois bonbons et le jeu s'arrête. Elle a gagné au total 5 bonbons.

  L'objet de l'exercice est de calculer l'espérance de la variable aléatoire $X$ associée au nombre de bonbons gagnés à ce jeu. La formule vue en cours ne peut pas être utilisée car le nombre de lancers n'étant pas limité, l'univers est infini.

  On fait alors l'hypothèse suivante : sur un 1, plutôt que relancer le dé, on gagne $1+E(X)$, c'est-à-dire 1 plus le gain moyen obtenu sur le second lancer (qui est identique au gain moyen du jeu). Les valeurs prises par la variable aléatoire $X$ sont alors $\left\{1+E(X), 2, 3, 4, 5, 6\right\}$.

  \begin{enumerate}
    \item Dresser la loi de probabilité de $X$.
    \item \label{q:equation} En utilisant la formule de l'espérance vue en cours, montrer que $E(X)=\frac{21+E(X)}{6}$.
    \item Résoudre l'équation pour déterminer $E(X)$.
  \end{enumerate}
  Pour être rigoureux, avec cette méthode, nous n'avons pas prouvé que $E(X)$ est égale à la valeur trouvée à la question précédente : nous avons seulement montré que \emph{si l'espérance $E(X)$ existe}, elle est égale à cette valeur.
  \begin{enumerate}
      \setcounter{enumi}{3}
    \item \emph{Difficile.} Modifier cette expérience aléatoire, ou en inventer une nouvelle, telle que la résolution de l'équation obtenue par la même méthode qu'à la question \ref{q:equation} ne donne pas un résultat correct. Expliquer l'erreur en termes non mathématiques : Quelles caractéristiques de l'expérience aléatoire font que le résultat n'est pas correct ?
  \end{enumerate}
\end{exercice}

\begin{exercice}[QCM]
  % Inspiré de l'exercice 56 p. 283 du manuel Sésamath 1S, 2015.
  Un exercice est composé de quatre questions à choix multiple. Chaque question a trois propositions de réponses, dont une seule est correcte.

  Une réponse correcte rapporte 2 points ; une réponse incorrecte en enlève 1. Un score final négatif est ramené à zéro.

  Un élève répond au hasard à ce QCM ; on note $X$ la variable aléatoire associée au nombre de points gagnés.

  \begin{enumerate}
    \item
        L'arbre de probabilité ci-dessous (dans lequel $J$ signifie « Réponse juste » et $F$ signifie « Réponse fausse »), qui décrit l'expérience aléatoire composée de la succession des réponses aux quatre questions, est incomplet : il ne décrit que (partiellement) les deux première questions, et il manque les probabilités.

        \begin{multicols}{2}
      Recopier et compléter cet arbre :
      \begin{enumerate*}
      \item tracer les branches manquantes ;
      \item écrire les probabilités ;
      \item écrire, pour chaque branche, le nombre de réponses correctes, et sa probabilité.
      \end{enumerate*}

      \columnbreak

    \begin{center}
      \begin{tikzpicture}[grow=right, sloped]
        % Set the overall layout of the tree
        \tikzstyle{level 1}=[level distance=1.5cm, sibling distance=2cm]
        \tikzstyle{level 2}=[level distance=2cm, sibling distance=1cm]
        \node {}
        child {
          node {F}
          child {
            node {…}
            edge from parent node[midway, below]{…}
          }
          child {
            node {…}
            edge from parent node[midway, above]{…}
          }
          edge from parent node[midway, below]{…}
        }
        child {
          node {J}
          child {
            node {F}
            edge from parent node[midway, below]{…}
          }
          child {
            node {J}
            edge from parent node[midway, above]{…}
          }
          edge from parent node[midway, above]{…}
        };
      \end{tikzpicture}
    \end{center}
      \end{multicols}
    \item En utilisant l'arbre de la question précédente, recopier et compléter le tableau suivant, pour déterminer la loi de probabilité de $X$.
      \begin{center}
        \begin{tabular}{l*{5}{|>{\centering\arraybackslash}p{1cm}}}
          Réponses correctes &0&1&2&3&4\\
        \hline
        Nombre de points &&&&& \\
        \hline
        Probabilité &&&&& \\
      \end{tabular}
      \end{center}
    \item Calculer l'espérance et la variance de $X$.
    \item Quelle est la note moyenne qu'obtiendraient un grand nombre de candidats répondant au hasard ?
  \end{enumerate}
\end{exercice}

\begin{exercice}[Valeurs remarquables]
  Le but de l'exercice est de calculer la valeur exacte de $\cos\frac{\pi}{12}$.

  \begin{multicols}{2}
  On considère le cercle trigonométrique, et on se place dans le repère orthonormé $\left( O, I, J \right)$. On considère les points $A$ et $B$, tels que $\left( \vecteur{OI}; \vecteur{OA} \right)=\frac{\pi}{4}$ et $\left( \vecteur{OI};\vecteur{OB} \right)=\frac{\pi}{3}$.


  \begin{center}
    \begin{tikzpicture}[scale=2, ultra thick]
      \coordinate (O) at (0, 0);
      \coordinate (A) at (45:1);
      \coordinate (B) at (60:1);
      \begin{scope}
        \clip (-.2, -.2) rectangle (1.2, 1.2);
        \draw[-latex] (-1.1, 0) -- (1.2, 0);
        \draw[-latex] (0, -1.1) -- (0, 1.2);
        \draw (1, 0) node[above right]{$I$};
        \draw (0, 1) node[above left]{$J$};
        \draw (O) circle (1);
      \end{scope}
      \draw (O) node[below left]{$O$};
      \draw[dashed] (O) -- (A) node[above right]{$A$};
      \draw[dashed] (O) -- (B) node[above right]{$B$};
    \end{tikzpicture}
  \end{center}
\end{multicols}

  \begin{enumerate}
    \item 
      \begin{enumerate}
        \item Rappeler les valeurs exactes de $\cos\frac{\pi}{4}$, $\sin\frac{\pi}{4}$, $\cos\frac{\pi}{3}$, et $\sin\frac{\pi}{3}$.
        \item Déterminer les coordonnées de $A$ et $B$, puis des vecteurs $\vecteur{OA}$ et $\vecteur{OB}$.
        \item En utilisant l'expression algébrique du produit scalaire (celle qui utilise les coordonnées des vecteurs), montrer que $\vecteur{OA}.\vecteur{OB}=\frac{\sqrt{2}\left( \sqrt{3}+1 \right)}{4}$.
      \end{enumerate}
    \item 
      \begin{enumerate}
        \item Démontrer, de manière aussi rigoureuse que possible, que $\left( \vecteur{OA};\vecteur{OB} \right)=\frac{\pi}{12}$.
        \item Exprimer le produit scalaire $\vecteur{OA}.\vecteur{OB}$ en fonction de $\cos\frac{\pi}{12}$.
      \end{enumerate}
    \item En déduire la valeur exacte de $\cos\frac{\pi}{12}$.
  \end{enumerate}
\end{exercice}

\begin{exercice}[Culture]
  Donner un exemple de progrès mathématique (nouveau théorème, nouvelle conjecture, nouveau problème ouvert, etc.) réalisé \emph{après} votre naissance.
\end{exercice}
\end{document}
