%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2017-2019 Louis Paternault --- http://ababsurdo.fr
%
% Publié sous licence Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)
% http://creativecommons.org/licenses/by-sa/4.0/deed.fr
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Pour compiler :
%$ lualatex $basename

\documentclass[12pt]{article}

\usepackage{1920-pablo}
\usepackage{1920-pablo-devoir}
\usepackage{1920-pablo-paternault}
\usepackage[a5paper, margin=2cm]{geometry}

\setlength{\parindent}{0pt}

\title{Trigonométrie\\Suites}
\date{22/05/2019}
\classe{1\up{e}S}
\dsnum{DM \no8}

\pagestyle{empty}
\begin{document}

\maketitle

\begin{exercice}[Trigonométrie]~
  \begin{em}
  On considère un nombre $x$ dont on ne sait rien d'autre que $\sin x=\frac{\sqrt{5}}{3}$. On souhaiterai déterminer les valeurs exactes de $\cos x$ et $\cos2x$.
\end{em}

  \begin{enumerate}
    \item \emph{Montrer que $\cos x=\frac{2}{3}$.}
\begin{multicols}{2}
      Puisque $\sin^2x+\cos^2x=1$, alors : 
      \begin{align*}
      \cos^2x
      &=1-\sin^2x\\
      &= 1-\left( \frac{\sqrt{5}}{3} \right)^2\\
      &=1-\frac{5}{9}\\
      &=\frac{4}{9}
      \end{align*}
      Donc 
      $\cos x=\sqrt{\frac{4}{9}}$
      ou
      $\cos x=-\sqrt{\frac{4}{9}}$.
      Et c'est tout… Il y a une erreur d'énoncé : nous n'avons pas assez d'information pour conclure. Nous allons supposer, comme l'énoncé le suggère, que $\cos x=\sqrt{\frac{4}{9}}=\frac{2}{3}$.
\end{multicols}
    \item \emph{Déterminer la valeur de $\cos 2x$.}
      \begin{align*}
\cos2x
&= 2\cos^2x-1\\
&= 2\left( \frac{2}{3} \right)^2-1\\
&= 2\times\frac{4}{9}-1\\
&= -\frac{1}{9}
      \end{align*}
  \end{enumerate}
\end{exercice}

\begin{exercice}[Trigonométrie]~
  \begin{enumerate}
    \item \emph{Simplifier l'expression suivante.}
      \[
        A=\cos\frac{\pi}{5}\sin\frac{4\pi}{5}+
        \sin\frac{\pi}{5}\cos\frac{4\pi}{5}
      \]
      Puisque $\sin (a+b)=\cos a\sin b+\sin a\cos b$, alors :
      \begin{align*}
A
&=\cos\frac{\pi}{5}\sin\frac{4\pi}{5}+ \sin\frac{\pi}{5}\cos\frac{4\pi}{5} \\
&= \sin\left( \frac{\pi}{5}+\frac{4\pi}{5} \right)\\
      \end{align*}
    \item \emph{En déduire la valeur de $A$.}
      D'après la question précédente :
      \begin{align*}
        A
&= \sin\left( \frac{\pi}{5}+\frac{4\pi}{5} \right)\\
&= \sin\frac{5\pi}{5}\\
&=\sin\pi\\
&=0
      \end{align*}
  \end{enumerate}
\end{exercice}

\begin{exercice}[Variations de suites]
  \begin{em}
  On considère les suites $u$ et $v$ définies sur $\mathbb{N}$ par $u_n=\frac{3^n}{4}$ et $v_n=-n^2-2n$.
\end{em}
  \begin{enumerate}
    \item \emph{Étude de $u$.}
      \begin{enumerate}
        \item \emph{Exprimer $\frac{u_{n+1}}{u_n}$ en fonction de $n$.}
          \begin{align*}
            \frac{u_{n+1}}{u_n}
            &=\frac{\frac{3^{n+1}}{4}}{\frac{3^n}{4}} \\
            &= \frac{3^{n+1}}{3^n}\times\frac{4}{4}\\
            &=3
          \end{align*}
        \item \emph{En déduire que la suite $u$ est croissante sur $\mathbb{N}$.}
          Puisque pour tout $n\in\mathbb{N}$, on a $\frac{u_{n+1}}{u_n}=3$, alors $\frac{u_{n+1}}{u_n}>1$, donc la suite $u$ est strictement croissante.
      \end{enumerate}
    \item \emph{Étude de $v$.}
      \emph{Déterminer les variations de la fonction $f$ définie sur $\mathbb{R^+}$ par $f(x)=-x^2-2x$, puis en déduire les variations de $v$.}
      La fonction $f$ est décroissante sur $\mathbb{R}^+$ (c'est un trinôme du second degré, dont le sommet a pour abscisse $-\frac{-2}{2\times-1}=-1$). Donc la fonction $f$ est strictement décroissante. Puisque $v_n=f(n)$ (pour tout $n$ de $\mathbb{N}$), alors la suite $v$ est aussi strictement décroissante.
      \item \emph{Comparer $u_{9\times10^9}$ et $v_{9\times10^9}$.}
        D'une part, $u_1=\frac{3^1}{4}=0,75$, et $v_1=-1^2-2\times1=-3$ (donc $v_1>u_1$). D'autre part, la suite $u$ est croissante, et la suite $v$ est décroissante. Donc :
        \[
          v_{9\times10^9}
          <
          v_1<u_1
          <u_{9\times10^9}
        \]
        Donc 
          $v_{9\times10^9} <u_{9\times10^9}$.
  \end{enumerate}
\end{exercice}

\begin{exercice}[Suite arthmético-géométrique]
  \begin{em}
  Une bibliothèque possède \numprint{8000} livres. Chaque année, elle jette \SI{5}{\%} de ses ouvrages, obsolètes, et en achète 500 nouveaux.

  On appelle $u$ la suite définie sur $\mathbb{N}$ par « $u_n$ est le nombre d'ouvrages de la bibliothèque au début de l'année $2017+n$ » (ainsi, $u_0=8000$ est le nombre d'ouvrages en 2017, $u_1$ en 2018, etc.).
\end{em}
  \begin{enumerate}
    \item \emph{Montrer que $u_1=8100$ et $u_2=8195$.}

      La première année, on jette $5\%$ de 8000 livres (soit $5\times8000\div100=400$) et on en achète 500 nouveaux : il y a donc $u_1=8000-400+500=8100$ livres.

      La seconde année, on jette $5\%$ de 8100 livres (soit $5\times8100\div100=405$) et on en achète 500 nouveaux : il y a donc $u_1=8100-405+500=8195$ livres.
    \item \emph{Montrer que pour tout $n\in\mathbb{N}$, $u_{n+1}=0,95u_n+500$.}
      
      Supprimer $5\%$ revient à multiplier par $0,95$. Donc pour tout $n\in\mathbb{N}$, on a $u_{n+1}=0,95u_n+500$.
  \end{enumerate}
  \begin{em}
  On pose, pour tout $n\in\mathbb{N}$ : $v_n=u_n-10000$.
\end{em}
  \begin{enumerate}
      \setcounter{enumi}{2}
    \item \emph{Montrer que $v$ est une suite géométrique de premier terme $\numprint{-2000}$ et de raison $0,95$.}
      Pour tout $n\in\mathbb{N}$, on a :
      \begin{align*}
        v_{n+1}
        &=u_{n+1}-10000\\
        &=0,95u_n+500-10000\\
        &=0,95u_n-9500\\
        &=0,95\left( u_n-\frac{9500}{0,95} \right)\\
        &=0,95\left( u_n-10000 \right)\\
        &=0,95v_n
      \end{align*}
      Donc $v$ est une suite géométrique, de premier terme $v_0=u_0-10000=8000-10000=-2000$, et de raison $0,95$.
    \item \emph{En déduire le terme général de $v$.}
      La suite $v$ est géométrique, donc $v_n=-2000\times0,95^n$.
    \item \emph{Calculer $u_{10}$. Combien de livres possèdera la bibliothèque en 2027 ?}
      Puisque $v_n=-2000\times0,95^n$ et $v_n=u_n-10000$, alors $u_n=v_n+10000=10000-2000\times0,95^n$.

      Donc $u_{10}=10000-2000\times0,95^{10}\approx8803$. En 2027, la bibliothèque aura $8803$ livres.
  \end{enumerate}
\end{exercice}

\end{document}
